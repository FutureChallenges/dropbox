<?php


require_once("Crypt/GPG.php");

if (count($argv) < 3) {
	echo "Usage: $argv[0] <aeskey_file> <aes_encrypted_file>\n";
	exit(1);
}

$gpg = new Crypt_GPG(array( "binary" => "/sw/bin/gpg2"));

$gpg->addDecryptKey("Max Marcon <maxmarcon@gmx.net>");

$aeskey_file = $argv[1];
$aeskey_encrypted_file = $argv[2];

$aeskey = $gpg->decryptFile($aeskey_file);
//echo md5($aeskey);
//exit();
$fpi = fopen($aeskey_encrypted_file, "r");
$cipher = 'rijndael-256';
$mode = 'cbc';
$iv = fread($fpi, mcrypt_get_iv_size($cipher, $mode));

$opts = array('iv'=>$iv, 'key'=>$aeskey);
stream_filter_append($fpi, 'mdecrypt.' . $cipher, STREAM_FILTER_READ, $opts);

stream_copy_to_stream($fpi, STDOUT);

fclose($fpi);