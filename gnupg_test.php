<?php


$key = openssl_random_pseudo_bytes(32, $strong);
echo "$strong
$key";
$cipher = 'rijndael-256';
$mode = 'cbc';

$iv = mcrypt_create_iv(mcrypt_get_iv_size($cipher, $mode), MCRYPT_DEV_URANDOM);
$opts = array('iv'=>$iv, 'key'=>$key);


$fp = fopen('secret-file.enc', 'w');
fwrite($fp, $iv);
stream_filter_append($fp, 'mcrypt.' . $cipher, STREAM_FILTER_WRITE, $opts);
fwrite($fp, 'This is an encrypted file');
fclose($fp);


$fp = fopen('secret-file.enc', 'r');
$iv = fread($fp, 32);
stream_filter_append($fp, 'mdecrypt.' . $cipher, STREAM_FILTER_READ, array('iv' => $iv, 'key' => $key));
$data = rtrim(stream_get_contents($fp));
fclose($fp);

echo $data. "\n";



//putenv('GNUPGHOME=/Users/max/.gnupg');
$r = gnupg_init();

if (! 
	gnupg_addencryptkey($r, "9E38C5C616FABE387DA9E92D1DA4399D3A3B4760")) {

	throw new Exception("gnupg_addencryptkey(identifier, fingerprint) failed");
}
echo gnupg_encrypt($r, $hashed_key) . "\n";


