<?php

use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;


Route::filter('check_timestamps', function() {

	if (Auth::check()) {
		$user = Auth::user();
		// check if the user logged in before the last password change
		// if yes, we log him out and ask him to log in again

		// update last_seen timestamp for the user
		$user->timestamps = false;
		$user->last_seen = time();
		$user->save();
		$user->timestamps = true;
	}
});

Route::filter('check_tor', function($route, $request, $check_type = "exit") {


	if (Session::get('tor.error')) {
		// check if we should repeat the failed tor check
		// we repeat the check with probability 40%
		$lottery = Config::get('tor.lottery');
		if (mt_rand(1, $lottery[1]) >= $lottery[0]) {
			Log::info("tor check for url " . $request->fullUrl() . " skipped");
			return;
		}
	}

	$istor = 0;
	if ($check_type == "tor2web") {

		$istor = ($request->header('X-tor2web') ? 0 : 1);

	} else if ($check_type == "exit") {

		$istor = Tor::check();
	}

	Session::put('tor.warning', ($istor < 1));
	Session::put('tor.error', ($istor < 0));

    if (!Session::get('tor.warned') && $istor < 1) {
    	Session::put('tor.warned', 1);
    	Session::set('tor_modal_dialog', 1);
    	Session::set('modal_dialog', array(
    		"header" => ($istor == 0 ? trans('warning.notor_header') : trans('warning.notor_unsure_header')),
			"text" => trans('warning.notor_msg'),
			"type" => ($istor == 0 ? "danger" : "warning"),
			"actions" => array(
				"tor_download" => array(
					"url" => "https://www.torproject.org/download/download.html.en",
					"text" => trans('warning.download_tor'),
					"attributes" => array(
						"target" => "_blank",
					)
				)
			),
			"close_text" => trans('warning.browse_without_tor')
		));
	} else if (Session::get('tor_modal_dialog') == 1) {
		Session::forget('tor_modal_dialog');
		Session::forget('modal_dialog');
	}
});


Route::filter('file_upload_root', function($route, $request, $should_be_under_fileupload_root) {


	$under_file_upload_root = ends_with($request->getBasePath(), "fileupload");

	if ($under_file_upload_root != $should_be_under_fileupload_root) {

		if ($under_file_upload_root) {

			// We are requesting some non-upload method under the fileupload root... Redirect to normal root
			$newUrl = preg_replace('/(fileupload\/?)/', '', $request->fullUrl(), 1);
			return Redirect::to($newUrl);
		}

		// We are trying to upload under the nromal root... Error
		throw new AccessDeniedHttpException();
	}

});

Route::filter('form_traps', function() {

	if (Input::get('email2') ||
		Input::get('telephone') ||
		Input::get('name') ||
		Input::get('address')) {
		Log::info("FORM TRAPS TRIGGERED");
		return Redirect::route('home');
	}
});

Route::filter('remove_intended_url', function() {

	Session::forget('url.intended');
});

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	if (!isset($_ENV['GPG_BINARY']))
		throw new Exception("You have to specify the location of the GPG binary through the GPG_BINARY variable in the .env.php file");

});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('admin', function() {

	if (Auth::guest())
		return Redirect::guest("/login/admin");

	if (Auth::user()->username != "admin")
		throw new AccessDeniedHttpException();
});


Route::filter('auth', function($route)
{
	if (Auth::guest())
		return Redirect::guest('/login/user');

	$id = $route->getParameter('id');
	if ($id != null) {

		$user = Auth::user();
		if ($user->username != "admin" && $user->id != $id)
			throw new AccessDeniedHttpException();
	}

});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() !== Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException("csrf token mismatch");
	}
});