@extends('layouts.onecolumn')


@section('top')
	@if ($errors->any()) 
		<div class="alert alert-danger alert-dismissable">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>{{ trans('general.warning') }}</strong>
			{{ trans('users.login_failed') }}
		</div>
	@elseif (Session::has('url.intended'))
		<div class="alert alert-warning">
			{{ trans('users.login_instructions_redirect') }}
		</div>
	@else
		<div class="alert alert-info">
		  	<strong>{{ trans('general.welcome') }}</strong>
			{{ trans('users.login_instructions') }}
		</div>
	@endif
@stop

@section('main')

@if (isset($user) && $user)

{{ Form::open(array('url' => URL::route('doLogin', array('type' => 'user')), 'role' => 'form')) }}

{{-- Traps for evil bots --}}
{{ Form::hidden('name') }}
{{ Form::hidden('address') }}
{{ Form::hidden('email2') }}

<div class="form-group {{ $errors->any() ? 'has-error' : ''}}">
	{{ Form::label('password', trans('users.password_user')) }}
	{{ Form::password('password', array('class' => 'form-control')) }}
</div>

<div class="form-group">
	{{ Form::submit(trans('users.login_button'), array('class' => 'btn btn-primary')) }}
</div>

{{-- Traps for evil bots --}}
{{ Form::hidden('telephone') }}

{{ Form::close() }}

@endif

@if (isset($admin) && $admin)

<div class="alert alert-info">
		{{ trans('users.login_instructions_admin') }}
</div>

{{ Form::open(array('url' => URL::route('doLogin', array('type' => 'admin')), 'role' => 'form')) }}

{{-- Traps for evil bots --}}
{{ Form::hidden('name') }}
{{ Form::hidden('address') }}
{{ Form::hidden('email2') }}

<div class="form-group {{ $errors->any() ? 'has-error' : ''}}">
	{{ Form::label('password', trans('users.password_admin')) }}
	{{ Form::password('password', array('class' => 'form-control')) }}
</div>

<div class="form-group">
	{{ Form::submit(trans('users.login_button_admin'), array('class' => 'btn btn-default')) }}
</div>

{{-- Traps for evil bots --}}
{{ Form::hidden('telephone') }}

{{ Form::close() }}

@endif

@stop