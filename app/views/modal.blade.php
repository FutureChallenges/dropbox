
@if (!Input::has("no_dialog"))

      <div class="modal fade" id="modal_dialog" tabindex="-1" data-backdrop="static" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
           <div class="modal-header {{{ 'alert-' . (isset($type) ? $type : 'info') }}}">
	           <h2>{{{ (isset($header) ? $header : trans('general.warning_header') ) }}} </h2>
               <h4>{{{ (isset($text) ? $text : "") }}}</h4>
           </div>
           <div class="modal-body text-right {{{ 'alert-' . (isset($type) ? $type : 'info') }}}">
			     @if (isset($actions))
              @foreach ($actions as $action)
                <a type="button" class="btn btn-primary" href="{{{ $action['url'] }}}" {{ isset($action['attributes']) ? array_to_attr_list($action['attributes']) : "" }}>
                  {{{ $action['text'] }}}
                </a>
              @endforeach
            @endif
            @if (!isset($no_close))
            <a type="button" class="btn btn-default" data-dismiss="modal">
               {{{ isset($close_text) ? $close_text : trans('general.close') }}}
             </a>
             @endif
           </div>
          </div>
        </div>
      </div>

      @if (!isset($show_after_load) || $show_after_load)
      <script>
        $(window).load(function() {
          $('#modal_dialog').modal('show');
        });
      </script>
      @endif

<noscript>

 <div class="modal show" id="modal_dialog" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
           <div class="modal-header {{{ 'alert-' . (isset($type) ? $type : 'info') }}}">
             <h2>{{{ (isset($header) ? $header : trans('general.warning_header') ) }}} </h2>
               <h4>{{{ (isset($text) ? $text : "") }}}</h4>
           </div>
           <div class="modal-body text-right {{{ 'alert-' . (isset($type) ? $type : 'info') }}}">
           @if (isset($actions))
              @foreach ($actions as $action)
                <a type="button" class="btn btn-primary" href="{{{ $action['url'] }}}" {{ isset($action['attributes']) ? array_to_attr_list($action['attributes']) : "" }}>
                  {{{ $action['text'] }}}
                </a>
              @endforeach
            @endif
            @if (!isset($no_close))
            <a type="button" class="btn btn-default" href="{{{ add_query_string_to_current_url('no_dialog=1') }}}"> {{{ isset($close_text) ? $close_text : trans('general.close') }}}</a>
             @endif
           </div>
          </div>
        </div>
  </div>

</noscript>

@endif

