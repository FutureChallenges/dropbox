@extends('layouts.onecolumn')

@section('torwarning_dialog')
@stop

@section('torwarning_noscript')
@stop

@section('content')

 <div class="container">
      <div class="jumbotron {{ (Session::get('tor.error') ? 'alert-warning' : 'alert-danger') }}">
          @if (!Session::get('tor.error'))
              <h2>{{{ trans('warning.notor_header') }}} </h2>
              <h4 class="text-warning">{{{ trans('warning.notor_msg') }}}</h4>
          @else
              <h2>{{{ trans('warning.notor_unsure_header')}}} </h2>
              <h4 class="text-warning">{{{ trans('warning.notor_msg') }}}</h4>
          @endif
          {{ Form::open(array('route' => 'doOptout', 'class' => 'vspace-above-15', 'role' => 'form', 'id' => 'optoutForm')) }}
            <button type="submit" class="btn btn-default"> <?php echo e(trans('warning.browse_without_tor')); ?></button>
            <a type="button" href="https://www.torproject.org/download/" class="btn btn-primary">{{{ trans('warning.download_tor') }}} </a>
          {{ Form::close() }}
      </div>
 </div>

@stop