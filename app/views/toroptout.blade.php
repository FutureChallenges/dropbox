@extends('layouts.onecolumn')

@section('content')

<div class="container">
	<div class="jumbotron alert alert-danger">
		<h2>
			{{{ trans('warning.tor_optout_msg') }}}
		</h2>
@if (Session::has('url.intended')) 
		<p><a href="{{url(Session::get('url.intended'))}}">{{{ trans('general.click_to_continue') }}}</a>.</p>
@else
		<p>{{{ trans('general.next_page_suggestion') }}} <a href="{{URL::route('home')}}">{{{ trans('general.home_page') }}}</a>.</p>
@endif
	</div>
</div>
@stop