@extends('layouts.onecolumn')

@section('content')
	
	<div class="container-fluid">
	<h1>Oops! This is an Error</h1>
	<h2>{{{ $statusCode }}}: {{{ $statusText }}}</h2>
	<br>
	<h3>{{{ $errorMsg }}}</h3>

	</div>
@stop