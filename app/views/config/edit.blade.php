@extends('layouts.onecolumn')

@section('top')
	@if ($errors->any())
		<div class="alert alert-danger alert-dismissable">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  		  	<strong>{{ trans('general.warning') }}</strong>
			{{ trans('config.error_warning') }}
		</div>
	@else
		<div class="alert alert-info">
			{{{ trans('config.config_instructions') }}}
		</div>
	@endif
@stop

@section('main')

{{ Form::model($conf_object, array('id' => 'update_config', 'route' => 'updateConfig', 'role' => 'form', 'class' => 'form-horizontal')) }}

{{-- Traps for evil bots --}}
{{ Form::hidden('name') }}
{{ Form::hidden('address') }}
{{ Form::hidden('email2') }}

<div class="form-group {{ $errors->has('current_password') ? 'has-error' : ''}}">
	{{ Form::label('current_password', trans('config.current_password'), array('class' => 'text-right col-sm-2')) }}
	<div class="col-sm-10">
		{{ Form::password('current_password', array('class' => 'form-control', 'autocomplete' => 'off')) }}
	</div>
	<div class="col-sm-offset-2 col-sm-10">
		{{ $errors->first('current_password', '<label class="control-label" for="current_password">:message</label>') }}
	</div>
	<div class="col-sm-offset-2 col-sm-10">
		<span class="help-block">{{ trans('config.current_password_help') }}</span>
	</div>
</div>

{{-- Traps for evil bots --}}
{{ Form::hidden('email2') }}

<div class="row">
	<div class="col-sm-offset-2 col-sm-10">
		<div class="alert alert-info">
			<strong>{{{ trans('config.email_instructions') }}}</strong>
		</div>
	</div>
</div>


<fieldset class="email" {{ (Session::has('_old_input.disable_emails') ? (Input::old('disable_emails') ? "disabled" : "") : ($conf_object->disable_emails ? "disabled" : "")) }}>
	<div class="form-group {{ $errors->has('destination_email') ? 'has-error' : '' }}">
		{{ Form::label('destination_email', trans('config.destination_email'), array('class' => 'text-right col-sm-2')) }}
		<div class="col-sm-10">
			{{ Form::email('destination_email', null, array('class' => 'form-control' )) }}
		</div>
		<div class="col-sm-offset-2 col-sm-10">
			{{ $errors->first('destination_email', '<label class="control-label" for="destination_email">:message</label>') }}
		</div>
		<div class="col-sm-offset-2 col-sm-10">
			<span class="help-block">{{ trans('config.destination_email_help') }}</span>
		</div>
	</div>

	<div class="form-group">
		<div class="{{ $errors->has('smtp_host') ? 'has-error' : ''}}">
			{{ Form::label('smtp_host', trans('config.smtp_host'), array('class' => 'text-right col-sm-2')) }}
			<div class="col-sm-4">
				{{ Form::text('smtp_host', null, array('class' => 'form-control' )) }}
			</div>
		</div>

		<div class="{{ $errors->has('smtp_port') ? 'has-error' : ''}}">
			{{ Form::label('smtp_port', trans('config.smtp_port'), array('class' => 'text-right col-sm-2')) }}
			<div class="col-sm-2">
				{{ Form::text('smtp_port', null, array('class' => 'form-control' )) }}
			</div>
		</div>

		<div class="col-sm-offset-2 col-sm-4 {{ $errors->has('smtp_host') ? 'has-error' : ''}}">
			{{ $errors->first('smtp_host', '<label class="control-label" for="smtp_host">:message</label>') }}
		</div>
		<div class="col-sm-offset-2 col-sm-2 {{ $errors->has('smtp_port') ? 'has-error' : ''}}">
			{{ $errors->first('smtp_port', '<label class="control-label" for="smtp_port">:message</label>') }}
		</div>
	</div>


	<div class="form-group">

		{{ Form::label('smtp_host', trans('config.smtp_encryption'), array('class' => 'text-right col-sm-2')) }}
		<div class="col-sm-1">

			<div class="radio">
				<label>
					{{ Form::radio('smtp_encryption', 'ssl') }}
					{{ trans('config.smtp_encryption_ssl') }}
				</label>
			</div>
		</div>
		<div class="col-sm-1">
			<div class="radio">
				<label>
					{{ Form::radio('smtp_encryption', 'tls') }}
					{{ trans('config.smtp_encryption_tls') }}
				</label>
			</div>
		</div>

		<div class="col-sm-2 col-sm-offset-4">
			{{ Form::button(trans('config.set_standard_port'), array('class' => 'btn btn-default', 'disabled', 'onclick' => 'set_standard_port();' )) }}
		</div>
	</div>

	<div class="form-group">
		<div class="{{ $errors->has('smtp_username') ? 'has-error' : ''}}">
		{{ Form::label('smtp_username', trans('config.smtp_username'), array('class' => 'text-right col-sm-2')) }}
		<div class="col-sm-4">
			{{ Form::text('smtp_username', null, array('class' => 'form-control' )) }}
		</div>
		</div>

		<div class="{{ $errors->has('new_smtp_password') ? 'has-error' : ''}}">
		{{ Form::label('new_smtp_password', trans('config.new_smtp_password'), array('class' => 'text-right col-sm-2')) }}
		<div class="col-sm-4">
			{{ Form::password('new_smtp_password', array('class' => 'form-control', 'autocomplete' => 'off' )) }}
		</div>
		</div>

		<div class="col-sm-offset-2 col-sm-4 {{ $errors->has('smtp_username') ? 'has-error' : ''}}">
			{{ $errors->first('smtp_username', '<label class="control-label" for="smtp_username">:message</label>') }}
		</div>

		<div class="col-sm-offset-2 col-sm-4 {{ $errors->has('new_smtp_password') ? 'has-error' : ''}}">
			{{ $errors->first('new_smtp_password', '<label class="control-label" for="new_smtp_password">:message</label>') }}
		</div>

		{{ Form::label('new_smtp_password_confirmation', trans('config.new_smtp_password_confirmation'), array('class' => 'text-right col-sm-offset-6 col-sm-2')) }}
		<div class="col-sm-4">
			{{ Form::password('new_smtp_password_confirmation', array('class' => 'form-control', 'autocomplete' => 'off' )) }}
		</div>
	</div>


	<div class="form-group">

		<div class="{{ $errors->has('sender_name') ? 'has-error' : ''}}">
		{{ Form::label('sender_name', trans('config.sender_name'), array('class' => 'text-right col-sm-2')) }}
		<div class="col-sm-4">
			{{ Form::text('sender_name', null, array('class' => 'form-control' )) }}
		</div>
		</div>

		<div class="{{ $errors->has('sender_email') ? 'has-error' : ''}}">
		{{ Form::label('sender_email', trans('config.sender_email'), array('class' => 'text-right col-sm-2')) }}
		<div class="col-sm-4">
			{{ Form::email('sender_email', null, array('class' => 'form-control' )) }}
		</div>
		</div>

		<div class="col-sm-offset-2 col-sm-4 {{ $errors->has('sender_name') ? 'has-error' : ''}}">
			{{ $errors->first('sender_name', '<label class="control-label" for="sender_name">:message</label>') }}
		</div>

		<div class="col-sm-offset-2 col-sm-4 {{ $errors->has('sender_email') ? 'has-error' : ''}}">
			{{ $errors->first('sender_email', '<label class="control-label" for="sender_email">:message</label>') }}
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-2 col-sm-offset-2">
			{{ Form::button(trans('config.test_email'), array('id' => 'testEmailButton', 'onclick' => 'test_email();', 'class' => 'btn btn-default', 'disabled' )) }}
		</div>
	</div>


</fieldset>

<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
		<div class="checkbox">
		    <label>
				{{ Form::checkbox('disable_emails', 1, null, array(
				'onclick' => '$("fieldset.email").prop("disabled", this.checked);'
				)) }}
				{{ trans('config.disable_emails_help') }}
			</label>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-offset-2 col-sm-10">
		<div class="alert alert-info">
			<strong>{{{ trans('config.encryption_instructions') }}}</strong>
		</div>
	</div>
</div>

<div class="form-group {{ $errors->has('gpg_key') ? 'has-error' : '' }}">
	{{ Form::label('gpg_key', trans('config.gpg_key'), array('class' => 'text-right col-sm-2')) }}
	<div class="col-sm-10">
		{{ Form::textarea('gpg_key', /*Config::get('app.gpg_key')*/ null, array('class' => 'form-control')) }}
	</div>
	<div class="col-sm-offset-2 col-sm-10">
		{{ $errors->first('gpg_key', '<label class="control-label" for="email">:message</label>') }}
	</div>
	<div class="col-sm-offset-2 col-sm-10">
		<span class="help-block">{{ trans('config.gpg_key_help') }}</span>
	</div>
</div>

<div class="row">
	<div class="col-sm-offset-2 col-sm-10">
		<div class="alert alert-info">
			<strong>{{{ trans('config.password_change_instructions') }}}</strong>
		</div>
	</div>
</div>

<div class="form-group {{ $errors->has('new_password') ? 'has-error' : ''}}">
	{{ Form::label('new_password', trans('config.new_password'), array('class' => 'text-right col-sm-2')) }}
	<div class="col-sm-10">
		{{ Form::password('new_password', array('class' => 'form-control', 'autocomplete' => 'off')) }}
	</div>
	<div class="col-sm-offset-2 col-sm-10">
		{{ $errors->first('new_password', '<label class="control-label" for="password">:message</label>') }}
	</div>
	<div class="col-sm-offset-2 col-sm-10">
		<span class="help-block">{{ trans('config.password_help') }}</span>
	</div>
</div>

<div class="form-group {{ $errors->has('new_password') ? 'has-error' : ''}}">
	{{ Form::label('new_password_confirmation', trans('config.new_password_confirmation'), array('class' => 'text-right col-sm-2')) }}
	<div class="col-sm-10">
		{{ Form::password('new_password_confirmation', array('class' => 'form-control', 'autocomplete' => 'off')) }}
	</div>
</div>

<noscript>
{{  Form::hidden('static_confirm', 'update_config') }}
</noscript>


{{-- Traps for evil bots --}}
{{ Form::hidden('telephone') }}


<div class="form-group">
	<div class="col-sm-offset-2 col-sm-6">
		{{ Form::submit(trans('config.update_button'), array('class' => 'btn btn-primary ')) }}
		{{ Form::close() }}
	</div>
</div>

 @include('confirm', array('form_id' => 'update_config', 'text' => trans('config.really_update'), 'type' => 'warning', 'url' => URL::route('updateConfig') ))


{{ Form::open( array('role' => 'form', 'class' => 'form-horizontal', 'id' => 'reset_config', 'route' => 'resetConfig')) }}
<div class="row">
	<div class="col-sm-offset-2 col-sm-10">
		<div class="alert alert-info">
			<strong>{{{ trans('config.reset_instructions') }}}</strong>
		</div>
	</div>
</div>

<div class="form-group {{ $errors->has('current_password_r') ? 'has-error' : ''}}">
	{{ Form::label('current_password_r', trans('config.current_password'), array('class' => 'text-right col-sm-2')) }}
	<div class="col-sm-10">
		{{ Form::password('current_password_r', array('class' => 'form-control', 'autocomplete' => 'off')) }}
	</div>
	<div class="col-sm-offset-2 col-sm-10">
		{{ $errors->first('current_password_r', '<label class="control-label" for="current_password_r">:message</label>') }}
	</div>
</div>

<div class="form-group">
	<div class="col-sm-offset-2 col-sm-6">
		{{ Form::submit(trans('config.reset_button'), array('class' => 'btn btn-danger')) }}
	</div>
</div>

<noscript>
{{  Form::hidden('static_confirm', 'reset_config') }}
</noscript>
{{ Form::close() }}

@include('confirm', array('form_id' => 'reset_config', 'text' => trans('config.really_reset'), 'type' => 'danger', 'url' => URL::route('resetConfig') ))


<div class="modal fade" id="emailTestError" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
       <div class="modal-header alert-danger }}">
         <h2>Error</h2>
           <h4 id="emailTestErrorMessages"></h4>
       </div>
       <div class="modal-body text-right alert-danger">
        <button type="button" class="btn btn-primary" data-dismiss="modal">
           {{{ isset($ok_text) ? $ok_text : trans('general.ok') }}}
        </button>
       </div>
      </div>
    </div>
</div>

<div class="modal fade" id="emailTestOk" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
       <div class="modal-header alert-success }}">
         <h2>A test email was sent</h2>
       </div>
       <div class="modal-body text-right alert-success">
        <button type="button" class="btn btn-primary" data-dismiss="modal">
           {{{ isset($ok_text) ? $ok_text : trans('general.ok') }}}
        </button>
       </div>
      </div>
    </div>
</div>

@stop

@section('sidebar')
	<p>
		Some enlightening words from the bright minds behind irrepressible voices
	</p>
@stop

@section('footer')

@parent

<script>
var editConfig = {
	emailTestURL: '{{ URL::route('testEmail') }}'
}
</script>
{{ HTML::script("/resources/js/jquery.form.min.js") }}
{{ HTML::script('resources/js/config/edit.js') }}

@stop

