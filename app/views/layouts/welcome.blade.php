@extends('layouts.master')

{{-- Layoout with logo, navigation bar and a 3-areas content: top, main and sidebar --}}

@section('header')

@include('layouts.logo')

@include('layouts.navbar')

@stop

@section('content')

<div class="container-fluid">

  <div class="row">

    <div class="col-sm-12 vspace-above-15">


      <div class="jumbotron">
        <h2>Welcome to Irrepressible Voices</h2>
        <p>
          Irrepressible Voices is an anonymous, secure platform to upload videos about human right issues.
        </p>
      </div>

    </div>

  </div>

  <div class="row" style="margin-bottom:30px;">

    <div class="col-sm-12 vspace-above-15" style="text-align: center;">

      <h2>
          What do you want to do?

      </h2>

    </div>

  </div>


  <div class="row vspace-below-15">

    <div class="col-sm-6 vspace-above-15" style="text-align: center;">

        <a type="button" class="btn btn-primary" href="{{ URL::route('submission_upload', array('step' => 1)) }}"><h4>Upload a video</h4></a>

    </div>

    <div class="col-sm-6 vspace-above-15" style="text-align: center;">

        <a type="button" class="btn btn-primary" href="{{ URL::route('messages', array('action' => 'view')) }}"><h4>Check the status of a video I uploaded</h4></a>

    </div>

  </div>


  <div class="row" style="margin-top:30px; margin-bottom:100px;">

    <div class="col-sm-12 vspace-above-15" style="text-align: center;">
      <h3>{{ HTML::link(URL::route('submission_manager', array('action' => 'list')), trans('submissions.manager')) }}</h3>
    </div>

  </div>


</div>

@stop


