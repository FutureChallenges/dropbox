<div class="container-fluid">

<nav class="navbar navbar-default" role="navigation">

  <div class="navbar-header navbar-right">

  @if (Auth::check())
     {{ Form::open(array('route' => 'doLogout', 'role' => 'search', 'id' => 'logoutForm')) }}
      <a class="navbar-link hspace-left-15 hspace-right-5" href="{{ (Auth::user()->username == 'admin' ? URL::route('configuration') : URL::route('messages', array('action' => 'view'))) }}">{{ trans("navbar.logged_in_as") }}<strong>{{{Auth::user()->username}}}</strong></a>
      @if (Auth::user()->username == 'admin')
      <a class="navbar-btn btn btn-primary" href="{{{ URL::route('configuration') }}}"><span class="glyphicon glyphicon-user"></span> {{{ trans("navbar.configuration")}}}</a>      
      @else
      <a class="navbar-btn btn btn-primary" href="{{{ URL::route('messages', array('action' => 'view')) }}}">{{{ trans("navbar.my_video_status")}}}</a>      
      @endif
      <button type="submit" class="btn navbar-btn btn-default hspace-right-5">{{{ trans("navbar.logout")}}}</button>
  @else
     <a class="navbar-link hspace-left-15 hspace-right-5">{{{ trans("navbar.logged_out") }}}</a>
     <a class="navbar-btn btn btn-primary hspace-left-5 hspace-right-5" href="{{{ URL::to('login') }}}">{{{ trans("navbar.login")}}}</a>
  @endif
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
  
    @if (Auth::check())
       {{ Form::close() }}
    @endif
  </div>

  <div class="navbar-collapse collapse" id="navbar-collapse-1">
{{-- replace the above div element with this one to uncollapse the navbar by default:  <div class="navbar-collapse collapse in" id="navbar-collapse-1"> --}}

      <ul class="nav navbar-nav">
          <li><a href="{{ URL::route('home') }}">{{{ trans("navbar.home") }}}</a></li>
      </ul>
  </div>
</nav>

</div>