<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>{{{ trans('general.title') }}}</title>

    {{-- Bootstrap --}}
    {{ HTML::style("/resources/bootstrap/css/bootstrap.min.css") }}
    {{ HTML::style("/resources/css/utils.css") }}

    {{-- jQuery (necessary for Bootstrap's JavaScript plugins) --}}
    {{ HTML::script("/resources/js/jquery.min.js") }}
    {{-- Include all compiled plugins (below), or include individual files as needed --}}
    {{ HTML::script("/resources/bootstrap/js/bootstrap.min.js") }}

    {{-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries --}}
    {{-- WARNING: Respond.js doesn't work if you view the page via file:// --}}
    <!--[if lt IE 9]>
      {{ HTML::script("/resources/js/html5shiv.js") }}
      {{ HTML::script("/resources/js/respond.min.js") }}
    <![endif]-->
  </head>
  <body>

    <div class="page-wrap">

        @if (isset($modal_dialog))
          @include('modal', $modal_dialog)
        @endif

        @yield('header')

        @if (isset($js_required))
          <noscript>
            <div class="container">
              <div class="jumbotron alert-warning">
                <h2>{{{ trans('warning.js_disabled') }}}</h2>
                <h4>{{{ trans('warning.js_message') }}}</h4>
              </div>
            </div>
          </noscript>
        @endif

        @if (App::environment('development', 'local'))
        <div class="container-fluid">
            <div class="alert-warning alert">
              <h4>ENVIRONMENT: {{ App::environment() }}</h4>
            </div>
        </div>
        @endif


        @yield('content')

    </div>

    <footer class="site-footer">
    @section('footer')

      <div class="well well-sm" style="margin-bottom: 0px;">
      {{{ trans('general.copyright_note') }}}
      </div>
    @show
    </footer>

  </body>
</html>
