@extends('layouts.onecolumn')


@section('header')

@parent


@stop

@section('top')

<div class="page-header container-fluid">
  <h2>{{ trans('submissions.video_upload') }} <small>{{
   ($step == 'success' ? trans('submissions.completed') :
   ($step == 'canceled' ? trans('submissions.canceled') :
	   trans('submissions.video_upload_step', array('step'=> $step, 'total' => 2)))) }}</small></h2>
</div>


	@if ($errors->any())
		<div class="alert alert-danger alert-dismissable">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  		  	<strong>{{ trans('general.warning') }}</strong>
			{{ trans('submissions.error_warning') }}
		</div>
	@elseif ($step == 1)
		<div class="alert alert-info">
				{{ trans('submissions.upload_banner') }}
		</div>
	@endif

@stop


@section('main')

@if ($step == 1)


{{ Form::model($current_submission, array('id' => 'metadata_form', 'url' => URL::route('submission_upload', array('step' => $step, 'action' => 'submit')),  'enctype' => 'application/x-www-form-urlencoded', 'role' => 'form')) }}

@include('submissions.fields')

<div class="row">
	<div class="col-sm-12">
		<div class="form-group">
			{{ Form::submit( trans('submissions.to_video_upload'), array('class' => 'btn btn-primary')) }}
			{{ Form::close() }}
			{{ Form::open(array('url' => URL::route('submission_upload', array('step' => $step, 'action' => 'cancel')),'style' => 'display:inline;', 'role' => 'form')) }}
			{{ Form::submit( trans('submissions.video_cancel'), array('class' => 'btn btn-warning')) }}
			{{ Form::close() }}
		</div>
	</div>
</div>


@elseif ($step == 2)


{{ Form::open(array('url' => URL::to('fileupload/doUpload'), 'enctype' => 'multipart/form-data', 'role' => 'form', 'id' => 'videoUploadForm')) }}

<div class="row">
	<div class="col-sm-12">
		<div class="form-group">
			<h4>{{ trans('submissions.do_upload') }}</h4>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-4">
		<div class="form-group" id="nojs-file-upload">
				{{ Form::label('title', trans('submissions.upload_prompt_simple')) }}
				{{ Form::hidden('MAX_FILE_SIZE', string_to_bytes(ini_get('upload_max_filesize'))) }}
				{{ Form::file('submission_file', array('id' => 'fileInput', 'class' => 'btn btn-default')) }}
		</div>
	</div>
</div>
{{-- Form::file('submission_file', array('id' => 'fileInput', 'style' => 'display:none;')) --}}
<div class="row" id="videoDropGroup" style="display:none">
	<div class="col-sm-6">
		<div class="form-group">
			<div id="videoDrop" class="upload upload-select" >
				<div class="progress-area row">
					<div class="col-xs-11">
						<div class="progress">
						  <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
						   	0%
						  </div>
						</div>
					</div>
					<div class="col-xs-1">
						<button type="button" id="cancel-upload" class="close" aria-hidden="true">&times;</button>
					</div>
				   <h4 class="text-center"></h4>
				</div>
				<div class="message-area">
					<h4></h4>
					<div class="btn-group">
						<input id="ok-button" type="button" class="btn btn-sm btn-default" value="{{{ trans('submissions.ok') }}}"></input>
						<input id="retry-button" type="button" class="btn btn-sm btn-default" value="{{{ trans('submissions.retry') }}}"></input>
					</div>
				</div>
				<div class="upload-prompt">
					<img id="upload_icon" class="center-block" src="/resources/img/upload.png"></img>
					<h4 class="text-center"></h4>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			{{ Form::submit( trans('submissions.video_submit'), array('id' => 'video_submit', 'class' => 'btn btn-primary')) }}
		</div>
	</div>
</div>

{{ Form::close() }}


{{ Form::open(array('url' => URL::route('submission_upload', array('step' => $step, 'action' => 'back')), 'role' => 'form')) }}

<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			{{ Form::submit( trans('submissions.back'), array('class' => 'btn btn-primary nav-away-button')) }}
			{{ Form::close() }}
			{{ Form::open(array('url' => URL::route('submission_upload', array('step' => $step, 'action' => 'cancel')), 'style' => 'display:inline;', 'role' => 'form')) }}
			{{ Form::submit( trans('submissions.video_cancel'), array('class' => 'btn btn-warning nav-away-button')) }}
			{{ Form::close() }}
		</div>
	</div>
</div>



@elseif ($step == 'success')

<div class="jumbotron alert alert-success">
	<h2>
		{{{ trans('submissions.success') }}}
	</h2>
	<h3 class="test-submission-number">
		{{{ trans('submissions.submission_number', array('id' => $current_submission->id)) }}}
	</h3>
</div>

<div class="has-error">
	<p>{{{ trans('submissions.submission_password') }}}</p>
	<h3 class="text-center test-submission-password">{{Session::get('password')}}</h3>
	<p class="help-block">{{{ trans('submissions.submission_password_warning') }}}</p>
</div>


@elseif ($step == 'canceled')

<div class="jumbotron alert alert-warning">
	<h2>
		{{{ trans('submissions.canceled') }}}
	</h2>
	<p>{{{ trans('general.next_page_suggestion') }}} <a href="{{URL::route('home')}}">{{{ trans('general.home_page') }}}</a>.</p>
</div>


@endif

@stop

@section('footer')

@parent

@if ($step == 2)
<script>
var uploadConfig = {

	uploadXhr: null,
	uploadAjaxConfig: null,
  	uploadMaxSize: {{ string_to_bytes(ini_get('upload_max_filesize')) }},
	csrf_token: '{{ Session::getToken() }}',
	uploadURL: '{{ URL::to('fileupload/doUpload') }}',
	successURL: '{{ URL::route('submission_upload', array('step' => 'success')) }}',
	abortURL: '{{ URL::route('abort_upload') }}',

	uploadMsg: {
		aborted: '{{{ trans('submissions.upload_aborted') }}}',
		failed: '{{{ trans('submissions.upload_failed') }}}',
		uploading: '{{{ trans('submissions.uploading') }}}',
		upload_prompt: '{{{ trans('submissions.upload_prompt') }}}',
		wait: '{{{ trans('submissions.wait') }}}',
		max_size_exceeded: '{{{ trans('submissions.max_size_exceeded') }}}',
		upload_completed: '{{{ trans('submissions.upload_completed') }}}'
	}
}
</script>
{{ HTML::script("/resources/js/submissions/upload.js") }}
{{ HTML::script("/resources/js/jquery.form.min.js") }}
@endif

@stop


