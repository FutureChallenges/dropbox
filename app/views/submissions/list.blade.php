@extends('layouts.onecolumn')

@section('header')


@parent


<div class="page-header container-fluid">
  <h2>{{ trans('submissions.manager') }} <small>{{ trans('submissions.manager_help')}}</small></h2>
</div>

@stop

@section('top')

@stop

@section('main')

	@if (!$submissions->getTotal())
		<h4>{{ trans('submissions.manager_no_submissions') }}</h4><br>
	@else
		<h4>{{ trans('submissions.total_submissions', ['count' => $submissions->getTotal()]) }}</h4><br>

	{{ Form::open(array('id' => 'delete-submissions' , 'url' => URL::route('submission_manager', array('action' => 'delete', 'id' => NULL, 'from_page' => $submissions->getCurrentPage())))) }}

	<div class="table-responsive">
		<table class="table table-striped">
			<thead>
			<tr>
				<th></th>
				<th>{{{ trans('submissions.manager_title') }}}</th>
				<th>{{{ trans('submissions.manager_location') }}}</th>
				<th>{{{ trans('submissions.manager_time_recording') }}}</th>
				<th>{{{ trans('submissions.manager_time_submission') }}}</th>
				<th>{{{ trans('submissions.manager_size')}}}</th>
				<th>{{{ trans('submissions.manager_status') }}}</th>
				<th>{{{ trans('submissions.manager_id') }}}</th>
				<th></th>
			</tr>
			</thead>
			<tbody>
				@foreach ($submissions as $sub)
				<tr>
					<td>{{ Form::checkbox("delete[]", $sub->id) }}</td>
					<td><a href="{{ URL::route('submission_manager', array('action' => 'view', 'id' => $sub->id, 'from_page' => $submissions->getCurrentPage())) }}">{{{ $sub->title }}}</a></td>
					<td>{{{ $sub->location }}}</td>
					<td>{{{ $sub->time }}}</td>
					<td>{{{ $sub->created_at }}}</td>
					<td>{{{ bytes_to_string($sub->size) }}}</td>
					<td>{{{ trans('submissions.status_' . $sub->status) }}}</td>
					<td>{{{ $sub->id }}}</td>
					<td>
						<a title="{{{ trans('submissions.manager_help_download') }}}" class="btn btn-default" href="{{ URL::route('submission_manager', array('action' => 'download', 'id' => $sub->id, 'from_page' => $submissions->getCurrentPage() )) }}"><span class="glyphicon glyphicon-download"></span></a>
						<a title="{{{ trans('submissions.manager_help_edit') }}}" class="btn btn-default" href="{{ URL::route('submission_manager', array('action' => 'edit', 'id' => $sub->id, 'from_page' => $submissions->getCurrentPage() )) }}"><span class="glyphicon glyphicon-edit"></span></a>
						<a title="{{{ trans('submissions.manager_help_messages') }}}" class="btn btn-default" href="{{ URL::route('messages', array('action' => 'view', 'id' => $sub->id, 'from_page' => $submissions->getCurrentPage() )) }}"><span class="glyphicon glyphicon-envelope"></span></a>
					</td>
				</tr>
				@endforeach
				<tr>
					<td colspan="3">
						<div class="checkbox">
							<label>
								<input type="checkbox" onclick="toggle_select_all(this);">{{ trans('submissions.manager_select_all') }}
							</label>
						</div>
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</tbody>
		</table>
	</div>

	<div>
		{{ $submissions->links() }}
	</div>

	<noscript>
	{{  Form::hidden('static_confirm', 'delete-submissions') }}
	</noscript>
	<div class="form-group">
		<button type="submit" class="btn btn-warning">{{ trans('submissions.manager_help_delete') }} <span class="glyphicon glyphicon-trash"></span></button>
	</div>
	{{ Form::close() }}
	@include('confirm', array('text' => trans('submissions.manager_confirm_delete'), 'ok_text' => trans('general.yes'), 'cancel_text' => trans('general.no'), 'form_id' => 'delete-submissions', 'type' => 'warning', 'url' =>  URL::route('submission_manager', array('action' => 'delete' ))))

	<script>
		function toggle_select_all(button) {
			$('input[type="checkbox"][name="delete[]"]').prop("checked", $(button).prop("checked"));
		}
	</script>

	@endif

@stop

