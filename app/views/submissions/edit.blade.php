@extends('layouts.onecolumn')

@section('header')

@parent

<div class="page-header container-fluid">
  <h2>{{ trans('submissions.manager_editing', array('id' => $sub->id)) }} <small><a href="{{ URL::route('submission_manager', array('action' => 'list', 'id' => null, 'page' => $from_page)) }}">{{ trans('submissions.manager_to_list') }}</a></small></h2>
</div>


@stop


@section('top')



@if ($errors->any())
		<div class="alert alert-danger alert-dismissable">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  		  	<strong>{{ trans('general.warning') }}</strong>
			{{ trans('submissions.error_warning') }}
		</div>
	@elseif (isset($step) && $step == 1)
		<div class="alert alert-info">
				{{ trans('submissions.upload_banner') }}
		</div>
	@endif

@stop


@section('main')



@include('confirm', array('text' => trans('submissions.manager_update_confirm'), 'form_id' => 'metadata_form', 'type' => 'warning', 'url' => URL::route('submission_manager', array('action' => 'update', 'id' => $sub->id)) ))

{{ Form::model($sub, array('id' => 'metadata_form', 'role' => 'form', 'url' => URL::route('submission_manager', array('action' => 'update', 'id' => $sub->id, 'from_page' => $from_page)))) }}

@include('submissions.fields', array('no_checkbox' => TRUE, 'no_hints' => TRUE))

<noscript>
{{  Form::hidden('static_confirm', 'metadata_form') }}
</noscript>

<div class="row">
	<div class="col-sm-12">
		<div class="form-group">
			{{ Form::submit( trans('submissions.manager_update'), array('class' => 'btn btn-primary')) }}
			{{ Form::close() }}
			<a class='btn btn-default' href="{{ URL::route('submission_manager', array('action' => 'list', 'id' => null, 'page' => $from_page)) }}">{{ trans('submissions.manager_to_list') }}</a>
		</div>
	</div>
</div>

@stop