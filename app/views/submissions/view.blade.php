@extends('layouts.onecolumn')

@section('header')

@parent

<div class="page-header container-fluid">
  <h2>{{ trans('submissions.manager_viewing', array('id' => $sub->id)) }} <small><a href="{{ URL::route('submission_manager', array('action' => 'list', 'id' => null, 'page' => $from_page)) }}">{{ trans('submissions.manager_to_list') }}</a></small></h2>
</div>

@stop


@section('top')


@stop


@section('main')

{{ Form::model($sub, array( 'role' => 'form')) }}

@include('submissions.fields', array('no_checkbox' => TRUE, 'read_only' => TRUE, 'no_hints' => TRUE))

{{ Form::close() }}

<div class="row">
	<div class="col-sm-12">
		<div class="form-group">
			<a class='btn btn-primary' href="{{ URL::route('submission_manager', array('action' => 'edit', 'id' => $sub->id, 'from_page' => $from_page)) }}">{{ trans('submissions.manager_edit') }}</a>
			<a class='btn btn-default' href="{{ URL::route('submission_manager', array('action' => 'list', 'id' => null, 'page' => $from_page)) }}">{{ trans('submissions.manager_to_list') }}</a>
		</div>
	</div>
</div>

@stop