{{--
Control variables

read_only (flag)
no_checkbox (flag)
no_hints (flag)
--}}

{{-- Traps for evil bots --}}
{{ Form::hidden('name') }}
{{ Form::hidden('address') }}
{{ Form::hidden('email2') }}
{{ Form::hidden('telephone') }}

@if (!isset($no_hints))
<div class="row">
	<div class="col-sm-12">
		<div class="form-group">
			<h4>{{ trans('submissions.basic_info') }}</h4>
			<p>{{ trans('submissions.basic_info_desc') }}</p>
		</div>
	</div>
</div>
@endif

<div class="row">
	<div class="col-sm-12">
		<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
			{{ Form::label('title', trans('submissions.title')) }}
			{{ Form::text('title', null, array('class' => 'form-control', isset($read_only) ? 'disabled' : '')) }}
			{{ $errors->first('title', '<label class="control-label" for="title">:message</label>') }}
		</div>		
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<div class="form-group {{ $errors->has('location') ? 'has-error' : ''}}">
			{{ Form::label('location', trans('submissions.location')) }}
			{{ Form::text('location', null, array('class' => 'form-control', isset($read_only) ? 'disabled' : '' )) }}
			{{ $errors->first('location', '<label class="control-label" for="location">:message</label>') }}
		</div>
	</div>
	<div class="col-sm-6">
		<div class="form-group {{ $errors->has('time') ? 'has-error' : ''}}">
			{{ Form::label('time', trans('submissions.time')) }}
			{{ Form::text('time', null, array('class' => 'form-control', 'placeholder' => trans('submissions.date_format'), isset($read_only) ? 'disabled' : '') ) }}
			{{ $errors->first('time', '<label class="control-label" for="time">:message</label>') }}
		</div>
	</div>
</div>

@if (!isset($no_checkbox))
<div class="row">
	<div class="col-sm-12">
		<div class="form-group {{ $errors->has('terms_and_conditions') ? 'has-error' : ''}}">
			<div class="checkbox">
			    <label>
			      {{ Form::checkbox('terms_and_conditions') }} {{ trans('submissions.terms_and_conditions', array('link' => URL::route('terms') )) }}
			    </label>
			</div>
			{{ $errors->first('terms_and_conditions', '<label class="control-label" for="terms_and_conditions">:message</label>') }}
		</div>
	</div>
</div>
@endif

@if (!isset($no_hints))
<div class="row">
	<div class="col-sm-12">
		<div class="form-group">
			<h4>{{ trans('submissions.additional_info') }}</h4>
			<p>{{ trans('submissions.additional_info_desc') }}</p>
		</div>
	</div>
</div>
@endif

<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			{{ Form::label('description', trans('submissions.description')) }}
			{{ Form::textarea('description', null, array('class' => 'form-control', isset($read_only) ? 'disabled' : '' )) }}
		</div>
	</div>
	<div class="col-sm-6">
		<div class="form-group">
			{{ Form::label('statement', trans('submissions.statement')) }}
			{{ Form::textarea('statement', null, array('class' => 'form-control', isset($read_only) ? 'disabled' : '' )) }}
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			{{ Form::label('tags', trans('submissions.tags')) }}
			{{ Form::text('tags', null, array('class' => 'form-control', isset($read_only) ? 'disabled' : '' )) }}
		</div>
	</div>
	<div class="col-sm-6">
		<div class="form-group">
			{{ Form::label('category', trans('submissions.category')) }}
			{{ Form::select('category', $categories, null, array('class' => 'form-control', isset($read_only) ? 'disabled' : '' )) }}
		</div>
	</div>
</div>

