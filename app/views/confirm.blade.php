
{{-- 
Include this view like this:
    @include('confirm', array('form_id' => 'ID_OF_YOUR_FORM', 'type' => 'warning', 'url' => FORM_SUBMIT_URL ))

Add this to your form:

<noscript>
{{  Form::hidden('static_confirm', ID_OF_YOUR_FORM) }}
</noscript>

Your back end controller  need to respond to static_confirm 
(by flashing static_confirm in the session and repopulating the input),
and to cancel_submit, by repopulating the form without performing the action

 --}}

@if (Session::get('static_confirm') == $form_id)

<div class="modal show" id="{{ $form_id }}-confirm" data-backdrop="static" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
       <div class="modal-header {{ 'alert-' . (isset($type) ? $type : 'info') }}">
         <h2>{{{ (isset($header) ? $header : trans('general.warning_header') ) }}} </h2>
           <h4>{{{ (isset($text) ? $text : "") }}}</h4>
       </div>
       <div class="modal-body text-right {{ 'alert-' . (isset($type) ? $type : 'info') }}">
        {{ Form::open(array('url' => $url, 'style' => 'display: inline;')) }}
        @foreach (Session::getOldInput() as $name => $value)
          @if ($name != "_token" && $name != "static_confirm")
            @if (is_array($value))
            {{-- If the value is an array, we create a checkbox for each array element --}}
              @foreach ($value as $val)
                {{ Form::checkbox($name . "[]", $val, true, array("style" => "display:none;")) }}
              @endforeach
            @else
              {{ Form::hidden($name, $value) }}
            @endif
          @endif
        @endforeach                
        {{ Form::submit(isset($ok_text) ? $ok_text : trans('general.ok'), array('type' => 'submit', 'class' => 'btn btn-primary')) }}
        {{ Form::close() }}

        {{ Form::open(array('url' => $url, 'style' => 'display: inline;')) }}
        @foreach (Session::getOldInput() as $name => $value)
          @if ($name != "_token" && $name != "static_confirm")
           @if (is_array($value))
             {{-- If the value is an array, we create a checkbox for each array element --}}
              @foreach ($value as $val)
                {{ Form::checkbox($name . "[]", $val, true, array("style" => "display:none;")) }}
              @endforeach
            @else
              {{ Form::hidden($name, $value) }}
            @endif
          @endif
        @endforeach
        {{ Form::hidden('cancel_submit', TRUE) }}
        {{ Form::submit(isset($cancel_text) ? $cancel_text : trans('general.cancel'), array('type' => 'submit', 'class' => 'btn btn-default')) }}
        {{ Form::close() }}
       </div>
      </div>
    </div>
</div>


@else

 <div class="modal fade" id="{{ $form_id }}-confirm" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
       <div class="modal-header {{ 'alert-' . (isset($type) ? $type : 'info') }}">
         <h2>{{{ (isset($header) ? $header : trans('general.warning_header') ) }}} </h2>
           <h4>{{{ (isset($text) ? $text : "") }}}</h4>
       </div>
       <div class="modal-body text-right {{ 'alert-' . (isset($type) ? $type : 'info') }}">
        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="$('#{{ $form_id }}').submit();" >
           {{{ isset($ok_text) ? $ok_text : trans('general.ok') }}}
        </button>
        <button type="button" class="btn btn-default" data-dismiss="modal">  
           {{{ isset($cancel_text) ? $cancel_text : trans('general.cancel') }}}
         </button>
       </div>
      </div>
    </div>
</div>

<script>
$(window).load(function() {
  $('#{{ $form_id }}').find("input[type='submit'],button[type='submit']").first().click(function(){
    $('#{{ $form_id }}-confirm').modal('show');
    return false;
  });
});
</script>



@endif

