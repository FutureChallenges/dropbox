@extends('layouts.onecolumn')

@section('header')

@parent

<div class="page-header container-fluid">
  <h2>{{ trans('messages.header', array('id' => $sub->id)) }}</h2>
  <h2><small>{{ trans('messages.header_info') }}</small></h2>
</div>

@stop


@section('top')

@if (Auth::user()->username != "admin")

<p>
{{ trans('messages.messaging_info') }}
</p>

@endif

@stop


@section('main')

<div class="list-group">
	@foreach ($messages as $msg)
		@include('messages.single', array('msg' => $msg))
	@endforeach
</div>

{{ Form::open(array('id' => 'new-message-form', 'url' => URL::route('messages', array('action' => 'post', 'id' => $sub->id)), 'role' => 'form')) }}
<div class="row">
	<div class="form-group col-sm-10">
		{{ Form::label('message_content', trans('messages.write_message'), array('id' => 'type_message')) }}
		{{ Form::textarea('message_content', null, array('class' => 'form-control', 'rows' => '5')) }}
	</div>

	<div class="col-sm-2 vspace-above-25">

		<div class="row">
			<div class="form-group col-sm-12">
				<button title="{{ trans('messages.send_help') }}" type="submit" id="send_button" class="btn btn-primary">
				  <span class="glyphicon glyphicon-envelope"></span> {{ trans('messages.send') }}
				</button>
			</div>
		</div>

		<div class="row">
			<div class="form-group col-sm-12">
				{{ Form::button(trans('messages.clear'), array('title' => trans('messages.clear_help'), 'id' => 'clear_button', 'style' => 'display:none;', 'class' => 'btn btn-default')) }}
			</div>
		</div>
	</div>
</div>

{{ Form::close() }}

@stop

@section('footer')

@parent

{{ HTML::script('/resources/js/messages/main.js') }}

@stop