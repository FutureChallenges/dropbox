<div class="row">
	<div class="col-sm-8 {{ $msg->fromMe() ? 'col-sm-offset-4' : '' }}">

		<div class="list-group-item vspace-above-15 {{ ($msg->fromMe() ? 'list-group-item-success' : 'list-group-item-warning') }}">
		  <h4 class="list-group-item-heading">
		  	@if (Auth::user()->username == "admin")
		  		{{ $msg->dir == Message::FROM_USER ? trans('messages.admin_from_user', array('id' => $msg->submission))
		  		: trans('messages.admin_from_admin') }}
		  	@else
		  		{{ $msg->dir == Message::FROM_USER ? trans('messages.user_from_user')
		  		: trans('messages.user_from_admin') }}
		  	@endif
		  </h4>
		  <h5 class="list-group-item-heading sending-time" sent-prefix="{{trans('messages.sent_prefix')}}" ts="{{ $msg->created_at->timestamp }}">
		  		{{ trans('messages.sent', array('when' => $msg->created_at )) }}
		  </h5>
			</br>
		  <div class="list-group-item-text {{ (!$msg->fromMe() && !$msg->read ? ' bold-text' : '') }}">
		  	<pre>{{{ $msg->content }}}</pre>
		  </div>
		</div>

	</div>
</div>
