<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


Route::whenRegex('/^(?!doUpload)/', array('before' => 'check_tor:tor2web'));


// Cross-site request forgery protection and form traps for all post requests
// Exception: the final file upload, because we directly parse the POST request and thus
// check the CSRF token during parsing
Route::whenRegex('/^(?!doUpload)/', array('before' => 'csrf|form_traps'), array('post'));
Route::whenRegex('/^(?!doUpload)/', array('before' => 'file_upload_root:0'));



Route::get('', array('as' => 'home', 'uses' => 'HomeController@showWelcome'));


Route::post('doUpload', array('as' => 'file_upload', 'uses' => 'SubmissionController@fileUpload', 'before' => 'file_upload_root:1'));

Route::any('upload/{step}/{action?}', array('as' => 'submission_upload', 'uses' => 'SubmissionController@uploadProcess'));

Route::post('abortUpload', array('as' => 'abort_upload', 'uses' => 'SubmissionController@abortUpload'));


Route::any('terms_and_conditions', array('as' => 'terms', function() {

	return View::make('terms');
}));

Route::group(array('before' => 'guest'), function() {

	Route::get('login/{type?}', array('as' => 'login', 'uses' => 'UserController@loginForm') );

	Route::post('doLogin/{type?}', array('as' => 'doLogin', 'uses' => 'UserController@loginUser') );

});

Route::group(array('before' => 'auth'), function() {

	Route::post('doLogout', array('as' => 'doLogout', 'uses' => 'UserController@logoutUser') );

	Route::any('messages/{action}/{id?}', array('as' => 'messages', 'uses' => 'MessageController@manageMessages'));

});

Route::group(array('before' => 'admin'), function() {

	Route::any('submissions/{action}/{id?}', array('as' => 'submission_manager', 'uses' => 'SubmissionController@manageSubmissions' ));

	Route::get('configuration', array('as' => 'configuration', 'uses' => 'ConfigController@configPage') );

	Route::post('testEmail', array('as' => 'testEmail', 'uses' => 'ConfigController@testEmail') );

	Route::post('updateConfig', array('as' => 'updateConfig', 'uses' => 'ConfigController@updateConfig') );

	Route::post('resetConfig', array('as' => 'resetConfig', 'uses' => 'ConfigController@resetConfig') );
});


/*Route::any('/dosattack', function() {

	return View::make('dos');
});

Route::get('/test_mail', function() {

	send_notification('Test email');

});


Route::any('/test_warning', function() {

	return Response::view('test', array("modal_dialog" => array(
		"text" => "This is a warning ". DbConfig::get('app.gpg_key'),
		"type" => "danger",
		"show_after_load" => true,
		"actions" => array(
			"Run" => array(

				"url" => "http://www.google.de",
				"text" => "Run",
				"attributes" => array(
					"target" => "_blank",
					)
				)
			),
		"close_text" => "Dismiss"
		)));
});
*/


