<?php

use Illuminate\Session\TokenMismatchException;
use Symfony\Component\HttpKernel\Exception\HttpException;

use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

// Error handlers

App::error(function(TokenMismatchException $exception) {

	Log::error($exception);

	return build_response_for_http_exception(new HttpException(SymfonyResponse::HTTP_BAD_REQUEST, $exception->getMessage()));
});




if (!Config::get('app.debug')) {

	App::error(function(Exception $exception) {

		Log::error($exception);

		return build_response_for_http_exception(new HttpException(SymfonyResponse::HTTP_INTERNAL_SERVER_ERROR, "something went wrong"));

	});
}

App::error(function(HttpException $exception) {

	Log::error($exception);

	return build_response_for_http_exception($exception);
});

