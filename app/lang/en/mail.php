<?php

return array(

	'new_submission' => "Hello,\n\nA new submission has been uploaded with id :id.\n\nCleartext user password: :password.\n\n:descr Your secure dropbox",
	'new_message' => "Hello,\n\nA new message has arrived for submission with id :id.\n\nSent on: :time\n\nMessage text:\n\n:text\n\nYour secure dropbox",
	'error_during_submission' => "An error has occurred during submission.\n\nOccurred on: :time\n\nError: :error\n\nYour secure dropbox"
);