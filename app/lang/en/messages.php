<?php

return array(

	'header' => 'You are viewing the messages for submission number :id',
	'send' => 'Send',
	'send_help' => 'Send the message',
	'clear' => 'Clear',
	'clear_help' => 'Clear the text you typed',
	'no_messages' => 'No messages yet for this video',
	'write_message' => 'Type your message here:',
	'from-user' => 'From: You',
	'admin_from_user' => 'From: Uploader of submission :id',	
	'admin_from_admin' => 'From: Irrepressible Voices',
	'user_from_user' => 'From: You',	
	'user_from_admin' => 'From: Irrepressible Voices',
	'sent' => 'Sent on: :when (UTC)',
	'sent_prefix' => 'Sent on: ',
	'header_info' => 'If you wanted the messages for another video, log out, then log in again using the appropriate password',
	'messaging_info' => 'Your video has been successfully submitted and is being processed. Thank you! This page enables
you to stay in touch with us -- fully anonymously. You can post any messages or questions you have, we\'ll answer
them as quickly as possible. If you are expecting an answer from us, please check this page regularly: we do not have
your email address and therefore cannot contact you directly. Thank you!'

);