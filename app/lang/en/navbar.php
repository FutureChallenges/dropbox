<?php

return array(

	"home" => "Home",
	"videos" => "Videos",
	"community" => "Community",
	"news" => "News",
	"engage" => "Engage",
	"about" => "Who we are",
	"contact" => "Contact us!",
	"login" => "Log in",
	"logout" => "Log out",
	"signup" => "Sign up",
	"profile" => "My profile",
	"logged_in_as" => "Logged in as: ",
	"configuration" => "Configuration",
	"my_video_status" => "My submitted video",
	"logged_out" => "You are not logged in"
);