<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()	
	{
		return View::make('layouts.welcome');

	}

	/* despite our recommendations, you decided not to use the Tor browser to browser our website */
	public function optOut() {

		if (!Session::has('tor.warning')) {
			// we should not be here, let's redirect to home page
			return Redirect::route('home');
		}		

		Session::set('tor.optout', true);

		return Redirect::route('toroptout_success');
	}

	public function torWarning() {

		if (!Session::has('tor.warning')) {
			// we should not be here, let's redirect to home page
			return Redirect::route('home');
		}
		return View::make('torwarning'); 
	}

}