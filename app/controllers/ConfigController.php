<?php

use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

Validator::extend('is_correct_password',
			function($attribte, $value) {
				return Hash::check($value, Auth::user()->password);
			}
);

class ConfModel {

	static $mappings = array(
		"destination_email" => "app.destination_email",
		"smtp_host" => "mail.host",
		"smtp_port" => "mail.port",
		"smtp_username" => "mail.username",
		"sender_name" => "mail.from.name",
		"sender_email" => "mail.from.address",
		"disable_emails" => "mail.pretend",
		"gpg_key" => "gpg.key",
		"smtp_encryption" => "mail.encryption"
	);

	public function __construct(array $conf = null) {

		foreach (self::$mappings as $key => $val) {

			if (!$conf) {
				$this->$key = DbConfig::get($val);

			} else {
				if (isset($conf[$key])) {
					$this->$key = $conf[$key];
				}
			}
		}

		$this->setOriginalGPGKey();
	}

	private function setOriginalGPGKey() {

		if (isset($this->gpg_key)) {

			$gpg = create_gpg_object();
			$this->gpg_key = $gpg->exportPublicKey($this->gpg_key);
		}

	}

	public function update($input) {

		foreach (self::$mappings as $key => $val) {
			if (isset($input[$key])) {

				if ($key == 'gpg_key') {
					// import key into keyring and get fingerprint
					$val = import_gpg_key($input[$key]);
				} else {
					$val = $input[$key];
				}

				$this->$key = $val;
			}
		}
	}

	public function forget() {

		foreach (self::$mappings as $key => $val) {
			DbConfig::forget($val, App::environment());
		}
	}

	public function save() {

		foreach (self::$mappings as $key => $val) {
			DbConfig::store($val, $this->$key, App::environment());
		}
	}

	public function toArray(array $except=null) {

		$a = array();
		foreach (self::$mappings as $key => $val) {
			if ($except && !in_array($val, $except))
				$a[$val] = $this->$key;
		}
		return $a;
	}

}

class ConfigController extends BaseController {

	public function configPage() {

		return View::make('config.edit')->with('conf_object', new ConfModel());
	}

	private static function makeCleanInput() {
		return array_merge(
			    	Input::except(array('current_password', 'new_password', 'new_password_confirmation', 'new_smtp_password', 'new_smtp_password_confirmation',
			    		'disable_emails')),
			    	array(
			    		'disable_emails' => Input::has('disable_emails'),
			    		)
			    	);
	}

	public function updateConfig() {

		if (Input::get('static_confirm')) {

			return Redirect::route('configuration')->withInput(Input::all())
			->with('static_confirm', Input::get('static_confirm'));
		}

		if (Input::get('cancel_submit')) {

			return Redirect::route('configuration')->withInput(self::makeCleanInput());
		}

		Validator::extend('is_gpg_key',
			function($attribte, $value) {
				try {
					import_gpg_key($value);
				} catch(Exception $e) {
					Log::error($e->getMessage());
					return false;
				}
				return true;
			}
		);

		$rules = array(
			'current_password' => 'required|is_correct_password',
			'destination_email' => 'email',
			//'smtp_host' => '',
			'smtp_port' => 'numeric|between:1,65535',
//			'smtp_username' => '',
			'new_smtp_password' => 'confirmed',
		//	'sender_name' => 'mail.from.name',
			'sender_email' => 'email',
			'new_password' => 'confirmed|min:6',
			'gpg_key' => 'required|is_gpg_key',
			'smtp_encryption' => 'in:tls,ssl'
 		);

		$validator = Validator::make(Input::all(), $rules);

		$validator->sometimes(array('smtp_port', 'smtp_username', 'sender_email', 'sender_name', 'smtp_host', 'smtp_encryption', 'destination_email')
			, 'required', function($input) {
			return !($input->disable_emails);
		});

		if ($validator->fails()) {

			return Redirect::route('configuration')
		    	->withErrors($validator)
			    ->withInput(self::makeCleanInput());
		}


		$new_password = Input::get('new_password');
		if ($new_password) {
			$user = Auth::user();
			$user->password = Hash::make($new_password);
			Auth::refreshPasswordToken($user);
			// the password has been changed and the time of the change recorded
			// users who logged in before this time will be automatically logged out
			// however, this user should not! therefore, we log him in again
			Auth::login($user);
			$user->save();
		}

		$conf = new ConfModel();
		$conf->update(self::makeCleanInput());
		$conf->save();
		$new_smtp_password = Input::get('new_smtp_password');
		if ($new_smtp_password) {
			DbConfig::store('mail.password', $new_smtp_password, App::environment());
		}

		Session::flash('modal_dialog', array(
						"header" => trans('config.update_success'),
						"type" => "success",
						"show_after_load" => true,
						"close_text" => trans('general.ok')
						));

		return Redirect::route('configuration');
	}

	public function resetConfig() {

		if (Input::get('static_confirm')) {

			return Redirect::route('configuration')->withInput(Input::all())
			->with('static_confirm', Input::get('static_confirm'));
		}

		if (Input::get('cancel_submit')) {

			return Redirect::route('configuration');
		}


		$validator = Validator::make(Input::all(), array(
					'current_password_r' => 'required|is_correct_password'
					));

		if ($validator->fails()) {

			return Redirect::route('configuration')
		    	->withErrors($validator);
		}

		(new ConfModel())->forget();
		DbConfig::forget('mail.password', App::environment());

		Session::flash('modal_dialog', array(
						"header" => trans('config.reset_success'),
						"type" => "success",
						"show_after_load" => true,
						"close_text" => trans('general.ok')
						));

		return Redirect::route('configuration');
	}

	public function testEmail() {

		$rules = array(
			'destination_email' => 'required|email',
			'smtp_host' => 'required',
			'smtp_port' => 'required|numeric|between:1,65535',
			'smtp_username' => 'required',
			'new_smtp_password' => 'confirmed',
			'sender_name' => 'required',
			'sender_email' => 'required|email',
			'smtp_encryption' => 'required|in:tls,ssl'
 		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {

			$response = "<ul>";
			foreach ($validator->errors()->all() as $val) {
				$response .= "<li>" . e($val) . "</li>";
			}
			$response .= "</ul>";

			return Response::make($response, SymfonyResponse::HTTP_BAD_REQUEST);
		}

		$mail_conf = new ConfModel(Input::only(array('destination_email', 'smtp_port', 'smtp_host', 'smtp_username', 'sender_name', 'sender_email', 'smtp_encryption')));
		$mail_conf->disable_emails = false;

		$smtp_password = Input::get('new_smtp_password');
		if (!$smtp_password) {
			$smtp_password = DbConfig::get('mail.password');
		}
		$conf_array['mail.password'] = $smtp_password;

		try {
			send_encrypted_mail("TEST", $conf_array);
		} catch (Exception $e) {
			return Response::make("<p>" . e($e->getMessage()) . "</p>", SymfonyResponse::HTTP_BAD_REQUEST);
		}
	}

}