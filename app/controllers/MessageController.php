<?php

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class MessageController extends BaseController {


	public function manageMessages($action, $id=null) {

		if ($action == "post" && !Request::isMethod('post')) {
			throw new MethodNotAllowedHttpException(array('post'));
		}

		// try to automatically infer the submission id from the logged-in user
		if (!$id) {

			$user = Auth::user();
			if ($user->id == "admin") {
				throw new BadRequestHttpException();
			}

			$id = $user->id;
		}

		$sub = Submission::find($id);

		if ($sub) {
			if ($action == "view") {

				$messages = Message::where('submission', $id)->orderBy('created_at')->get();

				foreach ($messages as $msg) {
					if (!$msg->read && !$msg->fromMe()) {
						$msg->read = true;
						$msg->save();
						$msg->read = false;
					}
				}

				return View::make('messages.main')->with(array('sub' => $sub, 'messages' => $messages));

			} else if ($action == "post") {

				$content = trim(Input::get('message_content'));
				if (!$content) {
					throw new BadRequestHttpException();
				}
				$direction = (Auth::user()->username == "admin" ? Message::TO_USER : Message::FROM_USER);

				$msg = new Message(array('submission' => $id, 'content' => $content,
					'dir' => $direction, 'read' => FALSE));
				$msg->save();

				if ($direction == Message::FROM_USER) {
					$notif = Lang::get('mail.new_message', array("time" => $msg->created_at, "id" => $msg->submission, "text" => $msg->content));
					send_notification($notif);
				}

				return Redirect::route('messages', ['action' => 'view', 'id' => $id]);
			}

			throw new BadRequestHttpException();
		}

		throw new NotFoundHttpException();
	}

}
