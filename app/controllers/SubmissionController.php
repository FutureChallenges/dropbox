<?php


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

use Carbon\Carbon;
use Local\RawInputReader;

class SubmissionController extends BaseController {


	public function __construct(RawInputReader $input_reader) {

		$this->input_reader = $input_reader;
	}

	public function manageSubmissions($action, $id=NULL) {

		if (in_array($action, array("update", "delete")) && !Request::isMethod('post')) {
			throw new MethodNotAllowedHttpException(array('post'));
		}

		if ($action == "delete") {

			if (Input::get('static_confirm')) {
				return Redirect::route('submission_manager', array('action' => 'list', 'id' => NULL, 'page' => Input::get('from_page')))
				->withInput()
				->with('static_confirm', Input::get('static_confirm'));
			}

			$delete = Input::get('delete');

			$really_delete = !Input::get('cancel_submit') && is_array($delete) && !empty($delete);

			if ($really_delete) {
				foreach ($delete as $id) {
					Submission::destroy($id);
					self::cleanUpSubmissionFiles($id);
					Log::info("deleted submission with id: " . $id);
				}

				$modal_dialog = array(
					"header" => trans('general.success'),
					"text" => trans('submissions.manager_successfully_deleted'),
					"type" => "success",
					"show_after_load" => true,
					"close_text" => trans('general.ok')
					);

				Session::flash('modal_dialog', $modal_dialog);
			}

			return Redirect::route('submission_manager', array('action' => 'list', 'id' => NULL, 'page' => ($really_delete ? 1 : Input::get('from_page'))));
		}


		$sub = NULL;
		if ($id) {

			$sub = Submission::find($id);

		}

		if ($sub) {


			if ($action == "view" && $id) {

				return View::make('submissions.view')->with([
					'sub' => $sub,
					'from_page' => Input::get('from_page')
					]);

			}

			if ($action == "edit" && $id) {

				return View::make('submissions.edit')->with([
					'sub' => $sub,
					'from_page' => Input::get('from_page')
					]);
			}

			if ($action == "update" && $id) {
				if (Input::get('static_confirm')) {
					return Redirect::route('submission_manager', array('action' => 'edit', 'id' => $id, 'from_page' => Input::get('from_page')))
				    ->withInput(Input::all())
					->with('static_confirm', Input::get('static_confirm'));
				}

				if (Input::get('cancel_submit')) {
					return Redirect::route('submission_manager', array('action' => 'edit', 'id' => $id, 'from_page' => Input::get('from_page')))
				    ->withInput(Input::all());
				}

				$date_format = Submission::TIME_FORMAT;

				$rules = array(
						'time' => "required|date_format:${date_format}",
						'category' => "in:" . implode(",", Submission::$categories),
				);

				$messages = array(
					'date_format' => 'The :attribute does not match the format ' . trans('submissions.date_format')
				);

				$validator = Validator::make(Input::all(), $rules, $messages);

				if ($validator->fails()) {

				    return Redirect::route('submission_manager', array('action' => 'edit', 'id' => $id, 'from_page' => Input::get('from_page')))
				    	->withErrors($validator)
					    ->withInput(Input::all());
				}


				$sub->update(Input::only(array(
					'title', 'location', 'time', 'description', 'statement', /*'tags',*/ 'category', 'time'
				)));


				$modal_dialog = array(
						"header" => trans('general.success'),
						"text" => trans('submissions.manager_successfully_updated'),
						"type" => "success",
						"show_after_load" => true,
						"close_text" => trans('general.ok')
						);

				Session::flash('modal_dialog', $modal_dialog);

				return Redirect::route('submission_manager', array('action' => 'edit', 'id' => $id, 'from_page' => Input::get('from_page')));
			}

			if ($action == "download" && $id) {

				$file_list = self::makeSubmissionFileNames($id);
				foreach ($file_list as $n => $file) {
					if (!file_exists($file)) {
						unset($file_list[$n]);
					}
				}

				$arch = get_temp_dir() . "/" . str_random(10) . ".zip";
				$zipcommand = escapeshellcmd("zip -r -j $arch " . implode(" ", array_map('escapeshellarg', $file_list)));
				exec($zipcommand, $output, $retval);
				Log::debug($zipcommand);
				if ($retval) {
					Log::error($zipcommand . " FAILED");
					Log::error(serialize($output));
					$modal_dialog = array(
						"header" => trans('general.error'),
						"text" => trans('submissions.manager_download_error'),
						"type" => "danger",
						"show_after_load" => true,
						"close_text" => trans('general.ok')
						);

					Session::flash('modal_dialog', $modal_dialog);
					return Redirect::route('submission_manager', array('action' => 'list', 'id' => NULL, 'page' => Input::get('from_page')));
				}
				// schedule removal of temporary archive file after 15 minutes
				Queue::later(Carbon::now()->addMinutes(15), 'CleanUpFile', array(
						"file" => basename($arch)
						));
				return Response::download($arch, "submission-" . $id . ".zip", array('Content-Type' => "application/octet-stream"));
			}
		} else if ($action == "list") {

			$paginated_submissions = Submission::orderBy('created_at', 'DESC')->paginate(Config::get('view.pagination_size'));
			return View::make('submissions.list')
			->with([
					"submissions" => $paginated_submissions
				]);
		}

		throw new NotFoundHttpException();
	}


	public function abortUpload() {

		$submission = Submission::find(Session::get('uploading_submission'));
		if (!$submission) {
			throw new NotFoundHttpException();
		}

		$submission->status = Submission::STATUS_UPLOAD_ABORTED;
		$submission->save();
	}

	public function fileUpload() {

		$submission = Submission::find(Session::get('uploading_submission'));
		if (!$submission) {
			throw new NotFoundHttpException();
		}

		if (Session::get('uploading_step') != 2) {
			return Redirect::route('submission_upload', array('step' => 1));
		}
		// if the upload was aborted before, we now forget about it
		$submission->status = Submission::STATUS_UNDER_SUBMISSION;
		$submission->save();


		$start = microtime(TRUE);
		$gpg_exit_status = $this->submissionParseMultidata($submission->id, $filename, $filesize, $stderr);
		$duration = microtime(TRUE) - $start;

		if (Session::token() !== Input::get('_token')) {
			throw new Illuminate\Session\TokenMismatchException("csrf token mismatch");
		}

		if (!$filename) {
			throw new Exception("could not find a file in the POST data");
		}

		if ($gpg_exit_status) {

			$gpg_error = "gpg terminated with exit status: " . $gpg_exit_status . " stderr: " . $stderr;

			send_notification(Lang::get("mail.error_during_submission", array('time' => Carbon::now(),
			'error' => $gpg_error)));

			throw new Exception($gpg_error);
		}

		Log::info("new submission id: " . $submission->id . " size: " . $filesize . " proc_time: " . $duration);

		$submission->size = $filesize;
		$password = random_password(10);
		$submission->user_password = Hash::make($password);
		$submission->save();

		$descr = self::makeSubmissionDescription($submission);
		$info_file = self::makeInfoFileName($submission->id);

		file_put_contents($info_file, encrypt_message($descr));

		send_notification(Lang::get("mail.new_submission", array('id' => $submission->id,
			'password' => $password,
			'descr' => $descr)));

		Session::set('password', $password);
		Session::set('uploading_step', 'success');

		if (Request::ajax() || Request::input('ajaxForm')) {

			return make_json_response(array(
				"statusCode" => SymfonyResponse::HTTP_OK,
				"msg" => Lang::get('submissions.upload_successful', array("file" => $filename))
			));
		} else {
			return Redirect::route('submission_upload', array('step' => 'success'));
		}


	}

	protected function clearUploadSessionData() {

		Session::forget('uploading_submission');
		Session::forget('uploading_step');
		Session::forget('password');
	}

	public function uploadProcess($step, $action="show") {

		$sub_id = Session::get('uploading_submission');
		$submission = ($sub_id ? Submission::find($sub_id) : NULL);

		if (($action != "show") && !Request::isMethod('post')) {
			throw new MethodNotAllowedHttpException(array('post'));
		}

		if ($action == "cancel") {

			Submission::destroy($sub_id);
			self::cleanUpSubmissionFiles($sub_id);
			$this->clearUploadSessionData();

			return Redirect::route('submission_upload', array('step' => 'canceled'));
		}

		$date_format = Submission::TIME_FORMAT;

		if ($step == 1) {

			// Can't edit submissions that have already been successfully submitted
			if ($submission && $submission->status == Submission::STATUS_SUBMITTED) {

				$sub_id = $submission = NULL;
				$this->clearUploadSessionData();

			} elseif ($action == "submit") {

				$rules = array(
					'title' => 'required',
					'location' => 'required',
					'time' => "required|date_format:${date_format}",
					'category' => "in:" . implode(",", Submission::$categories),
					'terms_and_conditions' => 'accepted'
				);

				$messages = array(
					'date_format' => 'The :attribute does not match the format ' . trans('submissions.date_format')
				);

				$validator = Validator::make(Input::all(), $rules, $messages);

				if ($validator->fails()) {

				    return Redirect::route('submission_upload', array('step' => 1))
				    	->withErrors($validator)
					    ->withInput(Input::all());
				}

				if (!$submission) {
					$submission = new Submission();
				}

				$submission->status = Submission::STATUS_UNDER_SUBMISSION;

				$submission->fill(
						Input::only('title', 'location', 'statement', 'description', 'category', 'time')
					);

				$submission->save();

				Session::set('uploading_submission', $submission->id);
				Session::set('uploading_step', 2);

				return Redirect::route('submission_upload', array('step' => 2));
			}

			// do nothing

		} else if ($step == 2) {

			if ($submission && $submission->status == Submission::STATUS_UPLOAD_ABORTED)		 {

				Session::set('uploading_step', 2);
			}

			if (Session::get('uploading_step') != 2 || $action == "back" ||
				($submission && $submission->status == Submission::STATUS_SUBMITTED)) {

				Session::set('uploading_step', 1);
				return Redirect::route('submission_upload', array('step' => 1));
			}

		} else if ($step == 'success') {

			$redirect_to_home_page = false;

			if (Session::get('uploading_step') != 'success') {

				$redirect_to_home_page = true;

			} else {

				if ($submission && $submission->status != Submission::STATUS_UPLOAD_ABORTED) {

					if ($submission->status != Submission::STATUS_SUBMITTED) {
						$submission->status = Submission::STATUS_SUBMITTED;
						$submission->save();
					}

					// Keep the success page available for 5 minutes
					if (Carbon::now()->diffInSeconds($submission->updated_at) > 300) {
						$redirect_to_home_page = true;
					}

				} else {
					$redirect_to_home_page = true;
				}

			}

			if ($redirect_to_home_page) {

				$this->clearUploadSessionData();
				return Redirect::route('home');
			}

		} else if ($step != 'canceled') {

			$this->clearUploadSessionData();
			return Redirect::route('home');
		}

		return View::make('submissions.upload')->with(
			array(
				'step' => $step,
				'date_format' => $date_format,
				'current_submission' => $submission
			)
		);

	}

	private static function makeSubmissionDescription($sub) {

		$desc = "";
		foreach ($sub->getAttributes() as $key => $val) {
			$desc .= $key . ":\n" . $val . "\n\n";
		}
		return $desc;
	}


	private static function makeVideFileName($id) {

		if (App::environment() == 'testing') {
			return '/dev/null';
		}

		$basename = get_submission_dir() . "/submission-" . $id;

		return $basename . ".gpg";
	}

	private static function makeInfoFileName($id) {

		if (App::environment() == 'testing') {
			return '/dev/null';
		}

		$basename = get_submission_dir() . "/submission-" . $id;

		return $basename. ".info.pgp";
	}

	private static function makeSubmissionFileNames($id) {

		return [
			self::makeVideFileName($id),
			self::makeInfoFileName($id)
		];
	}


	private static function cleanUpSubmissionFiles($id) {

		if (App::environment() != 'testing') {

			foreach (self::makeSubmissionFileNames($id) as $fname) {
				if (file_exists($fname)) {
					unlink($fname);
				}
			}
		}
	}


	private function submissionParseMultidata($id, &$filename, &$filesize, &$stderr) {

		$fpi = $this->input_reader->openStream();
		$fpo = NULL;
		$fperr = NULL;
		$enc_proc = NULL;

		$MAX_FILE_SIZE = string_to_bytes(ini_get('upload_max_filesize'));

		$boundary = NULL;
		$infile = FALSE;
		$fieldname = NULL;
		$header = FALSE;
		$total_bytes = 0;
		$filesize = 0;
		$stderr = '';

		while (!feof($fpi)) {

			$line = fgets($fpi, 1024*8);
			$total_bytes += strlen($line);

			if (!$boundary) {
				$boundary = trim($line);
			}

			if ($header) {

				if (trim($line) == FALSE) {
					$header = FALSE;

				} else {

					if (preg_match("/\sname=\"([^\"]*)\"/", $line, $matches)) {
						$fieldname = $matches[1];
					}

					if (preg_match("/\sfilename=\"([^\"]*)\"/", $line, $matches)) {

						$infile = TRUE;
						$buffered_line = NULL;
						if ($fieldname == "submission_file") {

							$filesize = 0;
							$filename = $matches[1];
							$outputfile = self::makeVideFileName($id);
							$fgpg = fopen($outputfile, 'w');
							if ($fgpg == FALSE) {
								throw new Exception("could not open file: " . $outputfile);
							}

							$descriptor = [
								0 => ['pipe', 'r'],
								1 => $fgpg,
								2 => ['pipe', 'w']
							];

							$gpg_homedir = Config::get('gpg.homedir');

							$cmd = escapeshellcmd(Config::get('gpg.binary')
								. ' -e --batch --trust-model always'
								. ($gpg_homedir ? ' --homedir ' . escapeshellarg($gpg_homedir) : '')
								. ' -r ' . escapeshellarg(DbConfig::get('gpg.key'))
								);
							$enc_proc = proc_open($cmd, $descriptor, $pipes);
							if (!$enc_proc) {
								throw new Exception("could not proc_open: " . $cmd);
							}

							$fpo = $pipes[0];
							$fperr = $pipes[2];
						}
					}
				}

			} else {

				if (preg_match("/^" . preg_quote($boundary) . "/", $line)) {

					if ($infile) {
						if ($buffered_line) {
							$buffered_line = preg_replace("/\r?\n$/", "", $buffered_line);
							if ($fpo) {
								fwrite($fpo, $buffered_line);
							}
							$filesize += strlen($buffered_line);
							if ($filesize > $MAX_FILE_SIZE) {
								throw new HttpException(SymfonyResponse::HTTP_BAD_REQUEST, Lang::get('submissions.max_size_exceeded'));
							}
							$buffered_line = NULL;
						}
					}

					$fieldname = NULL;
					$header = TRUE;
					$infile = FALSE;

				} else {

					if ($infile) {

						if ($buffered_line) {
							if ($fpo) {
								fwrite($fpo, $buffered_line);
							}
							$filesize += strlen($buffered_line);
							if ($filesize > $MAX_FILE_SIZE) {
								throw new HttpException(SymfonyResponse::HTTP_BAD_REQUEST, Lang::get('submissions.max_size_exceeded'));
							}
							$buffered_line = NULL;
						}

						$buffered_line = $line;

					} else {

						if ($fieldname) {
							Request::instance()->request->set($fieldname, trim($line));
						}
					}

				}
			}

		}

		fclose($fpi);
		if ($fpo) {
			fclose($fpo);
		}
		if ($fperr) {
			while (!feof($fperr)) {
				$stderr .= fgets($fperr);
			}
			fclose($fperr);
		}
		if ($enc_proc) {
			return proc_close($enc_proc);
		}
		return FALSE;
	}

}

