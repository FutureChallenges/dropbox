<?php

use Illuminate\Support\MessageBag;

class UserController extends Controller {


	public function loginForm($type="general") 
	{

		return View::make('users.login')->with(array(
			"admin" => ($type == "general" || $type == "admin"),
			"user" => ($type == "general" || $type == "user")
			));
	}

	public function loginUser($type="admin") {

		$password = Input::get('password');

		if (Auth::attempt(array('admin' => ($type == "admin"), 'password' => $password))) {
			$user = Auth::user();

			// check if password needs rehash and recompute hash if this is true
			if ($type == "admin" && Hash::needsRehash($user->password)) {
				$user->password = Hash::make($password);
				$user->save();
			}
			// was the user trying to get to a specific page without being logged in? If yes, redirect him
			// to the intended page now
			$default = (Auth::user()->username == "admin" ?
				URL::route('submission_manager', array('action' => 'list'))
				:
				URL::route('messages', array('action' => 'view'))
			);
			return Redirect::intended($default);
		}

		return Redirect::route('login', array('type' => $type))
			->withErrors(new MessageBag(array('login' => 'error')))
			->withInput(Input::except('password'));
	}

	public function logoutUser() {

		Auth::logout();

		Session::flash('modal_dialog', array(
						"header" => trans('users.logout_success'),
						"type" => "success",
						"show_after_load" => true,
						"close_text" => trans('general.ok')
						));

		return Redirect::route('home');
	}





}