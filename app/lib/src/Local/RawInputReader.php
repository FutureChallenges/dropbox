<?php namespace Local;

interface RawInputReader {

	public function openStream();
}