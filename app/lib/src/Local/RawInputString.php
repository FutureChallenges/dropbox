<?php namespace Local;

class RawInputString implements RawInputReader {

	public function __construct($data) {

		$this->data = $data;
	}

	public function openStream() {

		return fopen('data://text/plain;base64,' . base64_encode($this->data), 'r');
	}
}