<?php namespace Local;

class RawInputPHP implements RawInputReader {

	public function openStream() {

		return fopen("php://input", "r");
	}
}