<?php namespace Local\Auth;

use \Illuminate\Auth\UserInterface;

class EloquentUserProvider extends \Illuminate\Auth\EloquentUserProvider {
	
	/**
	 * Update the "password" token for the given user in storage.
	 *
	 * @param  \Illuminate\Auth\UserInterface  $user
	 * @param  string  $token
	 * @return void
	 */
	public function updatePasswordToken(UserInterface $user, $token)
	{
		$user->setAttribute($user->getPasswordTokenName(), $token);

		$user->save();
	}


}