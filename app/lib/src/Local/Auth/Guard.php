<?php namespace Local\Auth;

use \Illuminate\Auth\UserInterface;

class Guard extends \Illuminate\Auth\Guard {


	protected $user_id_path = "user_id";

	protected $password_token_path = "password_token";

	/**
	* tries to retrieve the user login information from either the session or the recaller cookie
	* updates $this->user. Returns true if user was found
	*
	* @return bool
	*/
	public function retrieveUser() {

		$data = $this->session->get($this->getName());
		$token = null;

		if (!$data) {
			$recaller = $this->getRecaller();
			if ($this->validRecaller($recaller)) {
				list($data, $token) = explode('|', $recaller, 2);
				$data = unserialize($data);
			}
		}

		if (!is_array($data))
			return false;

		$user_id = array_get($data, $this->user_id_path);
		$password_token = array_get($data, $this->password_token_path);

		if ($token) {
			$user = $this->provider->retrieveByToken($user_id, $token);				
		} else {
			$user = $this->provider->retrieveByIDWithPasswordToken($user_id, $password_token);
		}

		return !is_null($this->user = $user);
	}

	/**	
	 * Get the currently authenticated user.
	 *
	 * @return \Illuminate\Auth\UserInterface|null
	 */
	public function user()
	{
		if ($this->loggedOut) return;

		if (is_null($this->user))
		{
			$this->retrieveUser();
		}

		return $this->user;
	}


	/**
	 * Log a user into the application.
	 *
	 * @param  \Illuminate\Auth\UserInterface  $user
	 * @param  bool  $remember
	 * @return void
	 */
	public function login(UserInterface $user, $remember = false)
	{

		$this->createPasswordTokenIfDoesntExist($user);

		// If the user should be permanently "remembered" by the application we will
		// queue a permanent cookie that contains the encrypted user
		// identifier, the remember token (to make sure old sessions are expired when the user logs out) and 
		// the password token (to make sure old sessions are expired when the user changes password). 
		// We will then decrypt this later to retrieve the users.
		// Otherwise, we store the user id and login time in the session
		if ($remember) {
			$this->createRememberTokenIfDoesntExist($user);

			$this->queueRecallerCookie($user);
		} else {
			$this->updateSession($user);
		}

		// If we have an event dispatcher instance set we will fire an event so that
		// any listeners will hook into the authentication events and run actions
		// based on the login and logout events fired from the guard instances.
		if (isset($this->events))
		{
			$this->events->fire('auth.login', array($user, $remember));
		}

		$this->setUser($user);
	}

	/**
	 * Log the given user ID into the application.
	 *
	 * @param  mixed  $id
	 * @param  bool   $remember
	 * @return \Illuminate\Auth\UserInterface
	 */
	public function loginUsingId($id, $remember = false)
	{
		$this->login($user = $this->provider->retrieveById($id), $remember);

		return $user;
	}

	/**
	* Create an array with the login data
	*
	* @return array
	*/
	public function makeLoginData($user) {

		return array(
				$this->user_id_path => $user->id,
				$this->password_token_path => $user->getPasswordToken()
		);				
	}

	/**
	 * Update the session with the given ID.
	 *
	 * @param  string  $id
	 * @return void
	 */
	protected function updateSession( $user)
	{
		$this->session->put($this->getName(), $this->makeLoginData($user));

		$this->session->migrate(true);
	}

	/**
	 * Queue the recaller cookie into the cookie jar.
	 *
	 * @param  \Illuminate\Auth\UserInterface  $user
	 * @return void
	 */
	protected function queueRecallerCookie(UserInterface $user)
	{
		$value = serialize($this->makeLoginData($user)).'|'.$user->getRememberToken();

		$this->getCookieJar()->queue($this->createRecaller($value));
	}

	/**
	 * Refresh the password token for the user.
	 *
	 * @param  \Illuminate\Auth\UserInterface  $user
	 * @return void
	 */
	public function refreshPasswordToken(UserInterface $user)
	{
		$user->setPasswordToken($token = str_random(60));

		$this->provider->updatePasswordToken($user, $token);
	}

	/**
	 * Create a new password token for the user if one doesn't already exist.
	 *
	 * @param  \Illuminate\Auth\UserInterface  $user
	 * @return void
	 */
	protected function createPasswordTokenIfDoesntExist(UserInterface $user)
	{
		if (!$user->getPasswordToken())
		{
			$this->refreshPasswordToken($user);
		}
	}

}