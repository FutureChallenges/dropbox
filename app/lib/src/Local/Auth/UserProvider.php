<?php namespace Local\Auth;

use \Illuminate\Auth\UserInterface;
use \Illuminate\Auth\UserProviderInterface;
use \Illuminate\Hashing\HasherInterface;
use \DbConfig;

class UserProvider implements UserProviderInterface {
	

	/**
	 * The hasher implementation.
	 *
	 * @var \Illuminate\Hashing\HasherInterface
	 */
	protected $hasher;

	/**
	 * The submission model.
	 *
	 * @var string
	 */
	protected $submission_model;


	/**
	 * Create a new dropbox user provider.
	 *
	 * @param  \Illuminate\Hashing\HasherInterface  $hasher
	 * @param  string  $submission_model
	 * @return void
	 */
	public function __construct(HasherInterface $hasher, $submission_model)
	{
		$this->submission_model = $submission_model;
		$this->hasher = $hasher;
	}

	/**
	 * Retrieve a user by their unique identifier.
	 *
	 * @param  mixed  	$identifier
	 * @return \Illuminate\Auth\UserInterface|null
	 */
	public function retrieveById($identifier) {
		return null;
	}

	/**
	 * Retrieve a user by their unique identifier.
	 *
	 * @param  mixed  	$identifier
	 * @param  string 	password_token
	 * @return \Illuminate\Auth\UserInterface|null
	 */
	public function retrieveByIdWithPasswordToken($identifier, $password_token) {

		if ($identifier == "admin") {
			$admin = self::makeAdminUser();
			if ($admin->getPasswordToken() == $password_token) {
				return $admin;
			}

		} else {
			$submission = $this->createSubmissionModel()->find($identifier);
			if ($submission) {
				return self::makeSubmissionUser($submission);
			}
		}

		return null;
	}

	private static function makeAdminUser() {
		return new User(array(
					'username' => 'admin',
					'id' => 'admin',
					'password' => DbConfig::get('app.admin_password'),
					'password_token' => DbConfig::get('app.admin_password_token')
		));
	}

	private static function makeSubmissionUser($submission) {
		return new User(array(
						'id' => $submission->id,
						'username' => 'submission-' . $submission->id,
						'password' => $submission->user_password
					));
	}

	/**
	 * Retrieve a user by by their unique identifier and "remember me" token.
	 *
	 * @param  mixed  $identifier
	 * @param  string  $token
	 * @return \Illuminate\Auth\UserInterface|null
	 */
	public function retrieveByToken($identifier, $token) {
		return null;
	}

	/**
	 * Update the "password" token for the given user in storage.
	 *
	 * @param  \Illuminate\Auth\UserInterface  $user
	 * @param  string  $token
	 * @return void
	 */
	public function updatePasswordToken(UserInterface $user, $token)
	{
		$user->__set($user->getPasswordTokenName(), $token);

		$user->save();
	}

	/**
	 * Update the "remember me" token for the given user in storage.
	 *
	 * @param  \Illuminate\Auth\UserInterface  $user
	 * @param  string  $token
	 * @return void
	 */
	public function updateRememberToken(UserInterface $user, $token) {		
	}

	
	/**
	 * Retrieve a user by the given credentials.
	 *
	 * @param  array  $credentials
	 * @return \Illuminate\Auth\UserInterface|null
	 */
	public function retrieveByCredentials(array $credentials) {

		if (isset($credentials['admin']) && $credentials['admin']) {

			return self::makeAdminUser();
		}

		if (isset($credentials['password'])) {

			$submissions = $this->createSubmissionModel()->all();
			foreach ($submissions as $sub) {

				if ($this->hasher->check($credentials['password'], $sub->user_password)) {
					return self::makeSubmissionUser($sub);
				}
			}
		}

		return null;
	}

	/**
	 * Validate a user against the given credentials.
	 *
	 * @param  \Illuminate\Auth\UserInterface  $user
	 * @param  array  $credentials
	 * @return bool
	 */
	public function validateCredentials(UserInterface $user, array $credentials) {

		return $this->hasher->check($credentials['password'], $user->getAuthPassword());
	}


	private function createSubmissionModel()
	{
		$class = '\\'.ltrim($this->submission_model, '\\');

		return new $class;
	}
	

}