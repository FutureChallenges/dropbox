<?php namespace Local\Auth;

use \Illuminate\Auth\GenericUser;
use \DbConfig;
use \App;

class User extends GenericUser {
	

	public function getPasswordTokenName() 
	{
		return 'password_token';
	}

	public function getPasswordToken()
	{
		if ($this->username != "admin")
			return null;

		return $this->password_token;
	}

	public function setPasswordToken($value)
	{	
		if ($this->username == "admin") {
			$this->password_token = $value;
		}
	}

	public function save()
	{
		if ($this->username == "admin") {
			DbConfig::store("app.admin_password", $this->password, App::environment());
			DbConfig::store("app.admin_password_token", $this->password_token, App::environment());
		}
	}
}
