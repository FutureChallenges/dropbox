<?php namespace Local\Support\Facades;

use \Illuminate\Support\Facades\Facade;

/**
 * @see \Illuminate\Hashing\BcryptHasher
 */
class Tor extends Facade {

	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor() { return 'tor'; }

}