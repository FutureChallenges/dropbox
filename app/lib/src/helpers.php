<?php

use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

require_once("Crypt/GPG.php");


if (!function_exists('random_password')) {

	/**
	 * generate a random "telephone-number-like" password
	 * of given length that only uses decimal digits
	 *
	 */
	function random_password($length = 10) {

		if (!function_exists('openssl_random_pseudo_bytes')) {

			throw new Exception('random_password: opensss_random_pseudo_bytes is not available');
		}

		$bytes = str_split(openssl_random_pseudo_bytes($length));

		$password = '';

		foreach ($bytes as $byte) {

			$password .= ord($byte) % 10;
		}

		return $password;
	}

}


if (!function_exists('string_to_bytes')) {


	/**
	 * converts a string of the format
	 *
	 * (number_of_bytes)([gG] | [mM] | [kK])
	 * into the corresponding numeric value
	 *
	 * For example: 100k becomes 102400
	 */
	function string_to_bytes($str) {

	    if (!preg_match("/^(\d+)([gGmMkK])?$/", $str, $pieces)) {
	    	return 0;
	    }

	    $last = (count($pieces) > 2 ? strtolower($pieces[2]) : null);
	    $val = intval($pieces[1]);

	    switch($last) {
	        case 'g':
	            $val *= 1024;
	        case 'm':
	            $val *= 1024;
	        case 'k':
	            $val *= 1024;
	    }

	    return $val;
	}

}

if (!function_exists('bytes_to_string')) {

	function bytes_to_string($val) {

		$div = 1;
		$last = "b";
		$gb = pow(2,30);
		$mb = pow(2,20);
		$kb = pow(2,10);

		if ($val >= $gb) {

			$div = $gb;
			$last = "Gb";

		} else if ($val >= $mb) {

			$div = $mb;
			$last = "Mb";

		} else if ($val >= $kb) {

			$div = $kb;
			$last = "Kb";
		}

		return round(floatval($val)/$div, 1) . $last;
	}
}

if (!function_exists('send_notification')) {

	function send_notification($cleartext) {

		$enc = encrypt_message($cleartext);

		Queue::push('SendNotification', array('enc' => $enc));
	}

}

if (!function_exists('config_override')) {


	function config_override($key, array $config=null) {

		if (isset($config[$key])) {
			return $config[$key];
		}
		return DbConfig::get($key);
	}
}

if (!function_exists('send_encrypted_mail')) {

	function send_encrypted_mail($enc, array $config=null) {

		if ($config) {
			foreach ($config as $key => $val) {
				Config::set($key, $val);
			}
		} else {
			// we need to set all mail-related configuration
			// because the mailer doesn't use the DbConfig facade
			// But we don't do it if DbConfig::get('mail') is empty
			$mail_config = DbConfig::get('mail');
			if (!empty($mail_config)) {
				Config::set('mail', $mail_config);
			}
		}

		Config::set('mail.driver', 'smtp');

		Mail::send(

			array('text' => 'emails.plaintext'),
			array('text' => $enc),
			function($message) use ($config)
				{
					if (!(config_override('mail.pretend', $config))) {
					    $message
					    ->from(config_override('mail.from.address', $config), config_override('mail.from.name', $config))
					    ->to(config_override('app.destination_email', $config))
					    ->subject('Notification');
					}
				}
		);

	}

}

if (!function_exists('encrypt_message')) {

	function encrypt_message($text) {
		$gpg = create_gpg_object();
		$gpg->addEncryptKey(DbConfig::get('gpg.key'));
		return $gpg->encrypt($text);
	}
}

if (!function_exists('shred_files')) {

	function shred_files($files) {

		if (!is_array($files)) {
			$files = array($files);
		}
		$file_list = implode(" ", array_map('escapeshellarg', $files));

		$cmd = escapeshellcmd(Config::get('app.shredder')) . " " . $file_list . " >& /dev/null";
		exec($cmd, $output, $retval);
		if ($retval != 0) {
			Log::error("could not shred files " . $file_list);
			return FALSE;
		} else {
			return TRUE;
		}

	}
}

if (!function_exists('import_gpg_key')) {

	/**
	 * Imports a GPG key into the keyring and returns the fingerpring
	 *
	 * @var $key key data
	 *
	 * @return fingerpring
	 */
	function import_gpg_key($key) {

		$gpg = create_gpg_object();
		$import = $gpg->importKey($key);

		return $import['fingerprint'];
	}
}




if (!function_exists('create_gpg_object')) {

	function create_gpg_object() {

		$gpg_config = [
			'binary' => Config::get('gpg.binary'),
		];

		$gpg_homedir = Config::get('gpg.homedir');
		if ($gpg_homedir) {
			$gpg_config['homedir'] = $gpg_homedir;
		}

		$gpg = new Crypt_GPG($gpg_config);
		return $gpg;
	}
}

if (!function_exists('build_response_for_http_exception')) {

	function build_response_for_http_exception(HttpException $exception) {

		if (Request::ajax() || Request::input('ajaxForm')) {

			// pack all the information about the exception in a json structure
			$json_data = array(
					"statusCode" => $exception->getStatusCode(),
					"statusText" => SymfonyResponse::$statusTexts[$exception->getStatusCode()],
					"errorMsg" => $exception->getMessage()
				);

			return make_json_response($json_data, $exception->getStatusCode());

		} else {

			return Response::view('error', array(
					'statusCode' => $exception->getStatusCode(),
				 	'errorMsg' => $exception->getMessage(),
				 	'statusText' => SymfonyResponse::$statusTexts[$exception->getStatusCode()]
			 	), $exception->getStatusCode());
		}
	}
}

if (!function_exists('ajaxform_json_encode')) {

	function ajaxform_json_encode($data) {

		return '<textarea>' . e(json_encode($data)) . '</textarea>';
	}
}

if (!function_exists('make_json_response')) {

	function make_json_response($data, $statuscode=SymfonyResponse::HTTP_OK) {

		if (Request::ajax()) {
			return Response::json($data, $statuscode);
		} else {
			return Response::make(ajaxform_json_encode($data), $statuscode);
		}

	}
}

if (!function_exists('array_to_attr_list')) {

	function array_to_attr_list($input) {

		$res = '';
		foreach ($input as $key => $val) {
			$res .= ' ' . e($key) . '="' . e($val) . '"';
		}
		return $res;
	}
}

if (!function_exists('add_query_string_to_current_url')) {

	function add_query_string_to_current_url($query_string) {

		$url = URL::full();

		$pos = strpos($url, "?");
		if ($pos === FALSE || $pos == (strlen($url)-1)) {
			return $url . "?" . "$query_string";
		}
		return $url . "&" . $query_string;
	}
}

if (!function_exists('get_temp_dir')) {
	function get_temp_dir() {
		return storage_path() . "/tmp";
	}
}

if (!function_exists('get_submission_dir')) {
	function get_submission_dir() {
		return storage_path() . "/submissions";
	}
}
