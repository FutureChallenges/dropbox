<?php


class CleanUpFile {

	public function fire($job, $data) {

		$file = get_temp_dir() . "/" . $data['file'];
		if (file_exists($file)) {
			Log::debug("deleting temporary file: " . $file);
			unlink($file);
		}
		$job->delete();
	}
}
