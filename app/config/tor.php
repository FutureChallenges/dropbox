<?php


return array(

	// Probability with which we will repeat a failed Tor test at the next request

	'lottery' => array(40,100)
);