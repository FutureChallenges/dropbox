<?php

use Local\Auth\User;


class AdminTest extends TestCase {


	public function setUp() {

		parent::setUp();

		Session::start();
		$submission = new Submission([
			'title' => str_random(10),
			'status' => Submission::STATUS_UNDER_SUBMISSION,
			'location' => str_random(5),
			'time' => '31/12/1999'
			]);
 		$submission->save();
		$this->submission_id = $submission->id;
	}


	public function tearDown() {

		parent::tearDown();

		Submission::destroy($this->submission_id);
	}


	public function testDownloadWithNoAuthentication() {

		Route::enableFilters();

		$this->route('GET', 'submission_manager', ['action' => 'download', 'id' => $this->submission_id]);
		$this->assertRedirectedToRoute('login', ['type' => 'admin']);
	}


	public function testDownloadWithUserAuthentication() {

		Route::enableFilters();

		$user = new User(['id' => $this->submission_id, 'username' => 'submission-' . $this->submission_id]);
		$this->be($user);

		$this->setExpectedException('Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException');
		$this->route('GET', 'submission_manager', ['action' => 'download', 'id' => $this->submission_id]);
	}


	public function testDeleteWithNoAuthentication() {

		Route::enableFilters();
		Session::start();

		$params = [
			'_token' => Session::token(),
			'delete' => [$this->submission_id]
		];

		$this->route('POST', 'submission_manager', ['action' => 'delete'], $params);
		$this->assertRedirectedToRoute('login', ['type' => 'admin']);
	}

	public function testDeleteWithUserAuthentication() {

		Route::enableFilters();
		Session::start();

		$params = [
			'_token' => Session::token(),
			'delete' => [$this->submission_id]
		];

		$user = new User(['id' => $this->submission_id, 'username' => 'submission-' . $this->submission_id]);
		$this->be($user);

		$this->setExpectedException('Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException');
		$this->route('POST', 'submission_manager', ['action' => 'delete'], $params);
	}


	public function testListWithNoAuthentication() {

		Route::enableFilters();

		$this->route('GET', 'submission_manager', ['action' => 'list']);
		$this->assertRedirectedToRoute('login', ['type' => 'admin']);
	}


	public function testViewWithAuthentication() {

		Route::enableFilters();

		$user = new User(['id' => 'admin', 'username' => 'admin']);
		$this->be($user);

		$this->route('GET', 'submission_manager', ['action' => 'view', 'id' => $this->submission_id]);
		$this->assertResponseOk();
	}

	public function testViewWithUserAuthentication() {

		Route::enableFilters();

		$user = new User(['id' => $this->submission_id, 'username' => 'submission-' . $this->submission_id]);
		$this->be($user);

		$this->setExpectedException('Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException');

		$this->route('GET', 'submission_manager', ['action' => 'view', 'id' => $this->submission_id]);
	}


	public function testViewWithNoAuthentication() {

		Route::enableFilters();

		$this->route('GET', 'submission_manager', ['action' => 'view', 'id' => $this->submission_id]);
		$this->assertRedirectedToRoute('login', ['type' => 'admin']);
	}


}