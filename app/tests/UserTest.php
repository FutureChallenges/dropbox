<?php

use Local\Auth\User;

class UserTest extends TestCase {


	public function setUp() {

		parent::setUp();

		Session::start();
		$submission = new Submission([
			'title' => str_random(10),
			'status' => Submission::STATUS_UNDER_SUBMISSION,
			'location' => str_random(5),
			'time' => '31/12/1999'
			]);
 		$submission->save();
		$this->submission_id = $submission->id;
	}


	public function tearDown() {

		parent::tearDown();

		Submission::destroy($this->submission_id);
	}

	public function testAcccessSubmissionAsUser() {

		Route::enableFilters();

		$user = new User(['id' => $this->submission_id, 'username' => 'submission-' . $this->submission_id]);
		$this->be($user);

		$this->route('GET', 'messages', ['action' => 'view', 'id' => $this->submission_id]);
		$this->assertResponseOk();
	}

	public function testAcccessSubmissionAsUserImplicitly() {

		Route::enableFilters();

		$user = new User(['id' => $this->submission_id, 'username' => 'submission-' . $this->submission_id]);
		$this->be($user);

		$this->route('GET', 'messages', ['action' => 'view']);
		$this->assertResponseOk();
	}

	public function testAcccessSubmissionAsAdmin() {

		Route::enableFilters();

		$user = new User(['id' => 'admin', 'username' => 'admin']);
		$this->be($user);

		$this->route('GET', 'messages', ['action' => 'view', 'id' => $this->submission_id]);
		$this->assertResponseOk();
	}

	public function testAcccessSubmissionNoAuthentication() {

		Route::enableFilters();

		$this->route('GET', 'messages', ['action' => 'view', 'id' => $this->submission_id]);
		$this->assertRedirectedToRoute('login', ['type' => 'user']);
	}


	public function testAcccessSubmissionAsWrongUser() {

		Route::enableFilters();

		$wrong_id = $this->submission_id + 1;

		$user = new User(['id' => $wrong_id, 'username' => 'submission-' . $wrong_id]);
		$this->be($user);

		$this->setExpectedException('Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException');
		$this->route('GET', 'messages', ['action' => 'view', 'id' => $this->submission_id]);
		$this->assertResponseOk();
	}


	public function testAcccessNonexistentSubmission() {

		Route::enableFilters();

		$user = new User(['id' => 'admin', 'username' => 'admin']);
		$this->be($user);

		$this->setExpectedException('Symfony\Component\HttpKernel\Exception\NotFoundHttpException');
		$this->route('GET', 'messages', ['action' => 'view', 'id' => $this->submission_id + 1]);
	}


	public function testPostMessageAsUser() {

		Route::enableFilters();

		$user = new User(['id' => $this->submission_id, 'username' => 'submission-' . $this->submission_id]);
		$this->be($user);

		$params = [
			'message_content' => str_random(256),
			'_token' => Session::token()
		];


		$mesages_before = Message::where('submission', $this->submission_id)->count();
		$this->route('POST', 'messages', ['action' => 'post', 'id' => $this->submission_id], $params);

		$this->assertRedirectedToRoute('messages', ['action' => 'view', 'id' => $this->submission_id]);
		$this->assertEquals($mesages_before+1,Message::where('submission', $this->submission_id)->count());

		$last_message = Message::where('submission', $this->submission_id)->orderBy('created_at')->get()->last();

		$this->assertFalse((bool) $last_message->read);
		$this->assertEquals($params['message_content'], $last_message->content);
	}


	public function testPostMessageAsAdmin() {

		Route::enableFilters();

		$user = new User(['id' => 'admin', 'username' => 'admin']);
		$this->be($user);

		$params = [
			'message_content' => str_random(256),
			'_token' => Session::token()
		];


		$mesages_before = Message::where('submission', $this->submission_id)->count();
		$this->route('POST', 'messages', ['action' => 'post', 'id' => $this->submission_id], $params);

		$this->assertRedirectedToRoute('messages', ['action' => 'view', 'id' => $this->submission_id]);
		$this->assertEquals($mesages_before+1,Message::where('submission', $this->submission_id)->count());

		$last_message = Message::where('submission', $this->submission_id)->orderBy('created_at')->get()->last();

		$this->assertFalse((bool) $last_message->read);
		$this->assertEquals($params['message_content'], $last_message->content);
	}

}