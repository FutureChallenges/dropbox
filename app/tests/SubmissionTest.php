<?php

use Carbon\Carbon;

class SubmissionTest extends TestCase {

	const FILE_SIZE = 10000000;

	public function setUp() {

		parent::setUp();

		$submission = new Submission([
			'title' => str_random(10),
			'status' => Submission::STATUS_UNDER_SUBMISSION,
			'location' => str_random(5),
			'time' => '31/12/1999'
			]);
 		$submission->save();
		$this->submission_id = $submission->id;
		$this->session(['uploading_submission' => $submission->id]);

		$this->toDestroy = [$submission->id];
	}


	public function tearDown() {

		parent::tearDown();

		foreach ($this->toDestroy as $id) {
			Submission::destroy($id);
		}
	}


	public function makeFileContent() {

		return openssl_random_pseudo_bytes(self::FILE_SIZE);
	}

	public function assertUploadSessionDataEmpty() {

		$this->assertEmpty(Session::get('uploading_step'));
		$this->assertEmpty(Session::get('uploading_submission'));
		$this->assertEmpty(Session::get('password'));
	}


	public function testUploadStep1OnlyWithPOST() {

		$this->setExpectedException('Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException');
		$this->route('GET', 'submission_upload', ['step' => 1, 'action' => 'submit']);
	}


	public function testUploadStep2OnlyWithPOST() {

		$this->setExpectedException('Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException');
		$this->route('GET', 'submission_upload', ['step' => 1, 'action' => 'submit']);
	}


	public function testUploadStep1() {

		Route::enableFilters();

		$params = [
			'title' => str_random(10),
			'location' => str_random(5),
			'time' => '30/12/1999',
			'terms_and_conditions' => 1,
			'_token' => Session::token()
		];

		$this->route('POST', 'submission_upload', ['step' => 1, 'action' => 'submit'], $params);

		$sub = Submission::find($this->submission_id);

		$this->assertEquals(Submission::STATUS_UNDER_SUBMISSION, $sub->status);
		$this->assertEquals($params['title'], $sub->title);
		$this->assertEquals($params['location'], $sub->location);
		$this->assertEquals($params['time'], $sub->time);
		$this->assertEquals(2, Session::get('uploading_step'));
	}


	public function testUploadStep1NewSubmission() {

		Route::enableFilters();

		$this->flushSession();

		$params = [
			'title' => str_random(10),
			'location' => str_random(5),
			'time' => '30/12/1999',
			'terms_and_conditions' => 1,
			'_token' => Session::token()
		];

		$this->route('POST', 'submission_upload', ['step' => 1, 'action' => 'submit'], $params);

		$this->assertSessionHas('uploading_submission');
		$this->toDestroy[] = Session::get('uploading_submission');

		$sub = Submission::find(Session::get('uploading_submission'));

		$this->assertFalse(Session::get('uploading_submission') == $this->submission_id);
		$this->assertEquals(Submission::STATUS_UNDER_SUBMISSION, $sub->status);
		$this->assertEquals($params['title'], $sub->title);
		$this->assertEquals($params['location'], $sub->location);
		$this->assertEquals($params['time'], $sub->time);
		$this->assertEquals(2, Session::get('uploading_step'));
	}

	public function testUploadStep1Cancel() {

		Route::enableFilters();

		$params = [
			'_token' => Session::token()
		];

		$this->route('POST', 'submission_upload', ['step' => 1, 'action' => 'cancel'], $params);

		$this->assertEmpty(Submission::find($this->submission_id));
		$this->assertUploadSessionDataEmpty();
		$this->assertRedirectedToRoute('submission_upload', ['step' => 'canceled']);
	}


	public function testUploadStepWrongTimeFormat() {

		Route::enableFilters();

		$params = [
			'title' => str_random(10),
			'location' => str_random(5),
			'time' => '30/30/1999',
			'terms_and_conditions' => 1,
			'_token' => Session::token()
		];

		$this->route('POST', 'submission_upload', ['step' => 1, 'action' => 'submit'], $params);

		$sub = Submission::find($this->submission_id);

		$this->assertSessionHasErrors('time');
	}


	public function testUploadStep1WrongToken() {

		Route::enableFilters();

		$params = [
			'title' => str_random(10),
			'location' => str_random(5),
			'time' => '30/12/1999',
			'terms_and_conditions' => 1,
			'_token' => str_random(10)
		];

		$this->setExpectedException('Illuminate\Session\TokenMismatchException');

		$this->route('POST', 'submission_upload', ['step' => 1, 'action' => 'submit'], $params);
	}




	public function testUploadStep1AlreadySubmitted() {

		$sub = Submission::find($this->submission_id);
		$sub->status = Submission::STATUS_SUBMITTED;
		$sub->save();

		$this->route('GET', 'submission_upload', ['step' => 1]);

		$this->assertUploadSessionDataEmpty();
	}


	public function testUploadStep2Back() {

		Route::enableFilters();

		$this->session(['uploading_step' => 2]);

		$params = [
			'_token' => Session::token()
		];

		$this->route('POST', 'submission_upload', ['step' => 2, 'action' => 'back'], $params);

		$this->assertRedirectedToRoute('submission_upload', array('step' => 1));
		$this->assertEquals(1, Session::get('uploading_step'));
	}


	public function testUploadStep2Cancel() {

		Route::enableFilters();

		$params = [
			'_token' => Session::token()
		];

		$this->route('POST', 'submission_upload', ['step' => 2, 'action' => 'cancel'], $params);

		$this->assertEmpty(Submission::find($this->submission_id));
		$this->assertUploadSessionDataEmpty();
		$this->assertRedirectedToRoute('submission_upload', ['step' => 'canceled']);
	}


	public function testUploadStep2AlreadySubmitted() {

		$sub = Submission::find($this->submission_id);
		$sub->status = Submission::STATUS_SUBMITTED;
		$sub->save();

		$this->session(['uploading_step' => 2]);

		$this->route('GET', 'submission_upload', ['step' => 2]);

		$this->assertRedirectedToRoute('submission_upload', array('step' => 1));
		$this->assertEquals(1, Session::get('uploading_step'));
	}


	public function testUploadStep2TooEarly() {

		$this->route('GET', 'submission_upload', ['step' => 2]);

		$this->assertRedirectedToRoute('submission_upload', array('step' => 1));
		$this->assertEquals(1, Session::get('uploading_step'));
	}


	public function testSuccessPage() {

		$password = random_password(10);

		$this->session(['uploading_step' => 'success', 'password' => $password]);

		$this->route('GET', 'submission_upload', ['step' => 'success']);

		$crawler = $this->client->getCrawler();

		$this->assertResponseOk();
		$this->assertEquals(Submission::STATUS_SUBMITTED, Submission::find($this->submission_id)->status);
		$this->assertEquals(trans('submissions.submission_number', array('id' => $this->submission_id)), trim($crawler->filter('.test-submission-number')->text()));
		$this->assertEquals($password, trim($crawler->filter('.test-submission-password')->text()));
	}


	public function testSuccessPageTooEarly() {

		$this->session(['uploading_step' => 2]);

		$this->route('GET', 'submission_upload', ['step' => 'success']);

		$this->assertUploadSessionDataEmpty();
		$this->assertRedirectedToRoute('home');
	}

	public function testSuccessPageTooLate() {

		$sub = Submission::find($this->submission_id);
		$sub->status = Submission::STATUS_SUBMITTED;
		$sub->updated_at = Carbon::now()->subMinutes(5)->subSeconds(1);
		$sub->save();

		$this->session(['uploading_step' => 'success']);

		$this->route('GET', 'submission_upload', ['step' => 'success']);

		$this->assertUploadSessionDataEmpty();
		$this->assertRedirectedToRoute('home');
	}

	/**
	 * If someboody tries to upload a file from the ordinary applicaiton root (i.e. not the one under "fileupload")
	 * we generate an access denied HTTP error
	 */
	public function testFileUploadUnderNormalRootShouldFail() {

		Route::enableFilters();

		$this->setExpectedException('Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException');

		$this->route('POST', 'file_upload');
	}

	public function testFileUploadTooEarly() {

		$this->route('POST', 'file_upload');

		$this->assertRedirectedToRoute('submission_upload', ['step' => 1]);
	}

	public function testFileUpload() {

		$this->session(['uploading_step' => 2]);

		App::bind('Local\RawInputReader', function($app) {

			return new Local\RawInputString(
'------WebKitFormBoundaryMV7JfmxvX48SshEq
Content-Disposition: form-data; name="submission_file"; filename="randomfile.bin"
Content-Type: application/octet-stream

' . $this->makeFileContent() . '
------WebKitFormBoundaryMV7JfmxvX48SshEq
Content-Disposition: form-data; name="_token"

' . Session::token() . '
------WebKitFormBoundaryMV7JfmxvX48SshEq--');
		});

		$this->route('POST', 'file_upload');

		$sub = Submission::find($this->submission_id);
		$this->assertEquals(self::FILE_SIZE, $sub->size);
		$this->assertSessionHas('password');
		$this->assertEquals(10, strlen(Session::get('password')));
		$this->assertEquals('success', Session::get('uploading_step'));
		$this->assertRedirectedToRoute('submission_upload', array('step' => 'success'));
	}

	public function testFileAjax() {

		$this->session(['uploading_step' => 2]);

		App::bind('Local\RawInputReader', function($app) {

			return new Local\RawInputString(
'------WebKitFormBoundaryMV7JfmxvX48SshEq
Content-Disposition: form-data; name="_token"

' . Session::token() . '
------WebKitFormBoundaryMV7JfmxvX48SshEq
Content-Disposition: form-data; name="submission_file"; filename="randomfile.bin"
Content-Type: application/octet-stream

' . $this->makeFileContent() . '
------WebKitFormBoundaryMV7JfmxvX48SshEq--');
		});

		$this->route('POST', 'file_upload', [], [], [], ['HTTP_X-Requested-With' => 'XMLHttpRequest']);

		$sub = Submission::find($this->submission_id);
		$this->assertEquals(self::FILE_SIZE, $sub->size);
		$this->assertSessionHas('password');
		$this->assertEquals(10, strlen(Session::get('password')));
		$this->assertEquals('success', Session::get('uploading_step'));

		$json = json_decode($this->client->getResponse()->getContent());
		$this->assertInstanceOf('stdClass', $json);
		$this->assertEquals(200, $json->statusCode);
	}


	public function testFileUploadAjaxForm() {

		$this->session(['uploading_step' => 2]);

		App::bind('Local\RawInputReader', function($app) {

			return new Local\RawInputString(
'------WebKitFormBoundaryMV7JfmxvX48SshEq
Content-Disposition: form-data; name="_token"

' . Session::token() . '
------WebKitFormBoundaryMV7JfmxvX48SshEq
Content-Disposition: form-data; name="submission_file"; filename="randomfile.bin"
Content-Type: application/octet-stream

' . $this->makeFileContent() . '
------WebKitFormBoundaryMV7JfmxvX48SshEq--');
		});

		$this->route('POST', 'file_upload', [], ['ajaxForm' => 1], [], []);

		$sub = Submission::find($this->submission_id);
		$this->assertEquals(self::FILE_SIZE, $sub->size);
		$this->assertSessionHas('password');
		$this->assertEquals(10, strlen(Session::get('password')));
		$this->assertEquals('success', Session::get('uploading_step'));

		$crawler = $this->client->getCrawler();
		$json = json_decode($crawler->filter('textarea')->html());
		$this->assertInstanceOf('stdClass', $json);
		$this->assertEquals(200, $json->statusCode);
	}

	public function testFileUploadNoToken() {

		$this->session(['uploading_step' => 2]);

		App::bind('Local\RawInputReader', function($app) {

			return new Local\RawInputString(
'------WebKitFormBoundaryMV7JfmxvX48SshEq
Content-Disposition: form-data; name="submission_file"; filename="randomfile.bin"
Content-Type: application/octet-stream

' . $this->makeFileContent() . '
------WebKitFormBoundaryMV7JfmxvX48SshEq--');
		});

		$this->setExpectedException('Illuminate\Session\TokenMismatchException');
		$this->route('POST', 'file_upload');
	}

	public function testFileUploadNoFile() {

		$this->session(['uploading_step' => 2]);

		App::bind('Local\RawInputReader', function($app) {

			return new Local\RawInputString(
'------WebKitFormBoundaryMV7JfmxvX48SshEq
Content-Disposition: form-data; name="_token"

' . Session::token() . '
------WebKitFormBoundaryMV7JfmxvX48SshEq--');
		});

		$this->setExpectedException('Exception', 'could not find a file in the POST data');
		$this->route('POST', 'file_upload');
	}

}