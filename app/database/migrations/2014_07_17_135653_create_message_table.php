<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('messages', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';

			$table->increments('id');
			$table->tinyInteger('dir');
			$table->text('content');
			$table->boolean('read');	
			$table->integer('submission')->unsigned();
			$table->timestamps();
			$table->foreign('submission')->references('id')->on('submissions')->onDelete('cascade')->onUpdate('cascade');
			$table->index(array('submission', 'created_at'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('messages');
	}

}
