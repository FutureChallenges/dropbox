	<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubmissionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('submissions', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';

			$table->increments('id');
			$table->string('title', 255)->nullable();
			$table->string('location', 255)->nullable();
			$table->string('category', 255)->nullable();
			$table->datetime('time');
			$table->text('description')->nullable();
			$table->text('statement')->nullable();
			$table->tinyInteger('status');
			$table->string('user_password', 100)->nullable();
			$table->integer('size')->unsigned()->nullable();
			$table->timestamps();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('submissions');
	}

}
