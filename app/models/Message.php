<?php


class Message extends Eloquent {
	

	const FROM_USER = 0;
	const TO_USER = 1;

	protected $guarded = array('id', 'created_at', 'updated_at');

	public function fromMe() {

		if (Auth::user()) {
			if (Auth::user()->username == "admin") {
				return $this->dir == self::TO_USER;
			} else {
				return $this->dir == self::FROM_USER;
			}
		}

		return false;
	}
}