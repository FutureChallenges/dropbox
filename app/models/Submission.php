<?php

use Carbon\Carbon;

class Submission extends Eloquent {

	static $categories = array(
		NULL,
		"ArmedConflict",
		"ArmsControl",
		"ChildrensRights",
		"DetentionAndImprisonment",
		"Discrimination",
		"EconomicSocialAndCulturalRights",
		"EnforcedDisappearances",
		"FreedomOfExpression",
		"HumanRightsDefenders",
		"HumanRightsEducation",
		"Impunity",
		"IndigenousPeoples",
		"Poverty",
		"RefugeesMigrantsAndInternallyDisplacedPersons",
		"Religion",
		"Torture",
		"Other"
	);

	const STATUS_UNDER_SUBMISSION = 1;
	const STATUS_SUBMITTED = 2;
	const STATUS_UPLOAD_ABORTED = 3;

	const TIME_FORMAT = "d/m/Y";

	protected $guarded = array('id', 'created_at', 'updated_at');

	public function setTimeAttribute($value) {

		$this->attributes['time'] = Carbon::createFromFormat(self::TIME_FORMAT, $value)->hour(0)->minute(0)->second(0);
	}

	public function getTimeAttribute($value) {

		return (new Carbon($value))->format(self::TIME_FORMAT);
	}

	public function getDates() {

		return array_merge(parent::getDates(), array("time"));
	}


}