<?php

/*
|--------------------------------------------------------------------------
| Register The Laravel Class Loader
|--------------------------------------------------------------------------
|
| In addition to using Composer, you may use the Laravel class loader to
| load your controllers and models. This is useful for keeping all of
| your classes in the "global" namespace without Composer updating.
|
*/

/*
ClassLoader::addDirectories(array(

	app_path().'/commands',
	app_path().'/controllers',
	app_path().'/models',
	app_path().'/database/seeds',

));*/

/*
|--------------------------------------------------------------------------
| Application Error Logger
|--------------------------------------------------------------------------
|
| Here we will configure the error logger setup for the application which
| is built on top of the wonderful Monolog library. By default we will
| build a basic log file setup which creates a single file for logs.
|
*/

Log::useFiles(storage_path().'/logs/laravel.log');

/*
|--------------------------------------------------------------------------
| Application Error Handler
|--------------------------------------------------------------------------
|
| Here you may handle any errors that occur in your application, including
| logging them or displaying custom views for specific errors. You may
| even register several error handlers to handle different types of
| exceptions. If nothing is returned, the default error view is
| shown, which includes a detailed stack trace during debug.
|
*/


App::error(function(Exception $exception, $code)
{
	Log::error($exception);
});

require app_path() . '/errors.php';


/*
|--------------------------------------------------------------------------
| Maintenance Mode Handler
|--------------------------------------------------------------------------
|
| The "down" Artisan command gives you the ability to put an application
| into maintenance mode. Here, you will define what is displayed back
| to the user if maintenance mode is in effect for the application.
|
*/

App::down(function()
{
	return Response::make("Be right back!", 503);
});

/*
|--------------------------------------------------------------------------
| Require The Filters File
|--------------------------------------------------------------------------
|
| Next we will load the filters file for the application. This gives us
| a nice separate location to store our route and application filter
| definitions instead of putting them all in the main routes file.
|
*/

require app_path().'/filters.php';


Auth::extend('dropbox', function($app) {

	return new \Local\Auth\UserProvider($app['hash'], 'Submission');
});

// Deny all access to newly created files to all other users.
// But only if we are running the real app (no CLI artisan)!
if (!App::runningInConsole()) {
	umask(0077);

	// create admin account if there is no one yet
	if (!DbConfig::has('app.admin_password')) {
		DbConfig::store('app.admin_password', Hash::make('change me'), App::environment());
	}

	// Setting temporary directory
	$tmp_folder = get_temp_dir();
	$submission_folder = get_submission_dir();

	if (!file_exists($tmp_folder)) {
		mkdir($tmp_folder, 0700, TRUE);
	}
	if (!file_exists($submission_folder)) {
		mkdir($submission_folder, 0700, TRUE);
	}
}

// Setup view composers

View::composer('submissions.fields', function($view) {

	$categories = array();
	foreach (Submission::$categories as $cat_eng) {
		$categories[$cat_eng] = trans("submissions.categories." . $cat_eng);
	}
	$view->with('categories', $categories);
});

View::composer('layouts.master', function($view) {

	$view->with('modal_dialog', Session::get('modal_dialog'));
});

App::bind('Local\RawInputReader', 'Local\RawInputPHP');
