

function handleVideoFiles(files) {
	if (!files || files.length === 0) {
		return;
	}
	var file = files[0];

	if (!FormData) {
		return;
	}

	if (file.size && file.size > uploadConfig.uploadMaxSize) {
		showError(uploadConfig.uploadMsg.max_size_exceeded);
		return;
	}

	var formData = new FormData();
	formData.append("_token", uploadConfig.csrf_token);
	formData.append("submission_file", file);

	var fileName = file.name;

	uploadConfig.uploadXhr = $.ajax({
		url: uploadConfig.uploadURL,
		type: "POST",
		contentType: false,
		processData: false,
		data: formData,
		beforeSend: function(xhr) {
			$('.nav-away-button').attr("disabled", true);
			prepareUpload(fileName);
		},
		complete: function() {
			$('.nav-away-button').attr("disabled", false);
			uploadConfig.uploadAjaxConfig = this;
		},
		success: function(response) {
			var json = parseAjaxFormJSON(response);
			showSuccess(json.msg);
		},
		error: function(xhr, status, error) {
			var json = parseAjaxFormJSON(xhr.responseText);
			handleError(xhr.status, xhr.statusText, json.errorMsg, status === "abort");
		},
		xhr: function() {
            var xhr = $.ajaxSettings.xhr();
            if (xhr.upload) {
                xhr.upload.addEventListener('progress', function(event) {
                    var percent = 0;
                    var position = event.loaded || event.position; /*event.position is deprecated*/
                    var total = event.total;
                    if (event.lengthComputable) {
                        percent = Math.ceil(position / total * 100);
                    }
                    updateUploadBar(percent);
                }, false);
            }
            return xhr;
        }
	});

}

function fileInputChanged() {

	var fileName, fileSize;
	if (this.files && this.files.length > 0) {
		fileName = this.files[0].name;
		fileSize = this.files[0].size;
	} else {
		fileName = this.value.split('/').pop().split('\\').pop();
	}

	if (!fileName) {
		return;
	}

	if (fileSize && fileSize > uploadConfig.uploadMaxSize) {
		showError(uploadConfig.uploadMsg.max_size_exceeded);
		return;
	}

	$('#videoUploadForm').ajaxSubmit({
		uploadProgress: function(event, pos, total, perc) {
			updateUploadBar(perc);
		},
		url: uploadConfig.uploadURL + '?ajaxForm=1',
		beforeSend: function(xhr) {
			$('.nav-away-button').attr("disabled", true);
			uploadConfig.uploadXhr = xhr;
			prepareUpload(fileName);
		},
		//iframe: true,
		error: function(xhr, status, error) {
			var json = parseAjaxFormJSON(xhr.responseText);
			handleError(xhr.status, xhr.statusText, json.errorMsg, status === "aborted" || status === "abort");
		},
		complete: function() {
			$('.nav-away-button').attr("disabled", false);
		},
		success: function(response) {

			var json = parseAjaxFormJSON(response);
    		if (json.statusCode === 200) {
    			showSuccess(json.msg);
    		} else {
    			handleError(json.statusCode, json.statusText, json.errorMsg, false);
    		}

		}
	});
}

function parseAjaxFormJSON(str) {

	if (typeof(str) === "object") {
	   return str;
	}

	var json = {};

	if (!str) {
		return json;
	}

	try {
		json = $.parseJSON(str);
	} catch(e) {
		json = $.parseJSON($(str).text());
	}

	return json;
}


function handleError(statusCode, statusText, errorMsg, aborted, retry) {

	if (retry == undefined) {
		retry = !aborted;
	}

	var msg = (aborted ? uploadConfig.uploadMsg.aborted : uploadConfig.uploadMsg.failed +
		(errorMsg ? ': ' + errorMsg : ''));

	showError(msg, retry);
}

function cancelUpload(e) {
	if (uploadConfig.uploadXhr) {
		uploadConfig.uploadXhr.abort();
	}

	$.ajax({
		type: "POST",
		url: uploadConfig.abortURL,
		data: {
			"_token": uploadConfig.csrf_token
		}
	});
}

function updateUploadBar(perc, msg) {
	var percString = perc + "%";
	$('#videoDrop .progress-bar').attr('aria-valuenow', perc);
	$('#videoDrop .progress-bar').width(percString);
	$('#videoDrop .progress-bar').text(msg ? msg : percString);
}

function prepareUpload(fileName) {


	$('#videoDrop').unbind('click', selectFile);
	$('#videoDrop').unbind('drop', fileDropped);

	$('#videoDrop').attr('class', 'upload upload-active');

	$('#cancel-upload').show();

	$('#videoDrop .progress-area').show();
	$('#videoDrop .upload-prompt').hide();
	$('#videoDrop .message-area').hide();

	$('#videoDrop .progress-area h4').text(uploadConfig.uploadMsg.uploading + " " + fileName);
	$('#videoDrop .progress-bar').width("100%");
	$('#videoDrop .progress-bar').text(uploadConfig.uploadMsg.wait);
	$('#videoDrop .progress').addClass("progress-striped active");
	$('#videoDrop .progress-bar').attr('class', 'progress-bar');
}

function showError(msg, retry) {

	$('#videoDrop').unbind('click', selectFile);
	$('#videoDrop').unbind('drop', fileDropped);

	$('#videoDrop').attr('class', 'upload alert-danger');

	$('#videoDrop .progress-area').hide();
	$('#videoDrop .upload-prompt').hide();
	$('#videoDrop .message-area').show();

	if (retry) {
		$('#retry-button').show();
	} else {
		$('#retry-button').hide();
	}
	$('#videoDrop .message-area h4').text(msg);
}

function showSuccess(msg) {

	updateUploadBar(100, uploadConfig.uploadMsg.upload_completed);

	$('#videoDrop').unbind('click', selectFile);
	$('#videoDrop').unbind('drop', fileDropped);

	$('#videoDrop').attr('class', 'upload upload-active');

	$('#cancel-upload').hide();
	$('#videoDrop .progress').removeClass('progress-striped active');
	$('#videoDrop .progress-area h4').text(msg);
	$('#videoDrop .progress-bar').attr('class', 'progress-bar progress-bar-success');

	window.location.replace(uploadConfig.successURL);

}

function reinitUploadArea() {

	uploadConfig.uploadXhr = uploadConfig.uploadAjaxConfig = null;

	$('#videoDrop .upload-prompt').show();
	$('#videoDrop .message-area').hide();
	$('#videoDrop .progress-area').hide();
	$('#videoDrop').attr('class', 'upload upload-select');
	$('#videoDrop .upload-prompt h4').text(uploadConfig.uploadMsg.upload_prompt);
	$('#videoDrop').bind('click', selectFile);
	$('#videoDrop').bind('drop', fileDropped);

}

function selectFile() {

	$('#fileInput').click();
}

function fileDropped(e) {
	e.stopPropagation(); e.preventDefault();

	var dt = e.originalEvent.dataTransfer;
	if (dt && $.inArray("Files", dt.types) > -1) {
		handleVideoFiles(dt.files);
	}
}

function initUploadArea() {


	$('#nojs-file-upload').hide();
	$('#video_submit').hide();
	$('#videoDropGroup').show();

	$('#fileInput').change(fileInputChanged);
	$('#videoDrop').bind('dragenter', function(e) {
		e.stopPropagation(); e.preventDefault();
	});
	$('#videoDrop').bind('dragleave', function(e) {
		e.stopPropagation(); e.preventDefault();
	});
	$('#videoDrop').bind('dragover', function(e) {
		e.stopPropagation(); e.preventDefault();
		var dt = e.originalEvent.dataTransfer;
		if (dt) {
			if ($.inArray("Files", dt.types) > -1) {
    			dt.dropEffect = "copy";
    		} else {
    			dt.dropEffect = "move";
    		}
		}
	});
	$('#cancel-upload').bind('click', cancelUpload);
    $('#videoUploadForm').ajaxForm();

    $('#ok-button').bind('click', function(e) {
    	reinitUploadArea();
    	e.stopPropagation();
    });

    $('#retry-button').bind('click', function(e) {
    	if (uploadConfig.uploadAjaxConfig) {
			$.ajax(uploadConfig.uploadAjaxConfig);
    	} else {
    		$('#fileInput').trigger('change');
		}
    	e.stopPropagation();
    });

    reinitUploadArea();
}

$(window).load(function() {
	initUploadArea();
});