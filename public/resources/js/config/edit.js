function test_email() {


	$('#update_config').ajaxSubmit({

		url: editConfig.emailTestURL,
		data: {
    			"ajaxForm": 1
    	},
    	beforeSend: function() {
    		$('#testEmailButton').prop('disabled', true);
    	},
    	complete: function() {
    		$('#testEmailButton').prop('disabled', false);
    	},
    	error: function(xhr, status, error) {

    		$('#emailTestErrorMessages').html(xhr.responseText);

    		$('#emailTestError').modal('show');
		},
		success: function(response) {
    		$('#emailTestOk').modal('show');

		}
	});
}


function set_standard_port() {

	var ssl_port = 465;
	var tls_port = 587;

	$('input[name="smtp_port"]').val(
		$('input[value="ssl"]').is(':checked') ? ssl_port : tls_port
	);
}

$(window).load(function() {
	$('button').prop('disabled', false);
});