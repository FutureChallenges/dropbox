
$(window).load(function() {
	$('#send_button').prop('disabled', true);

	$('#clear_button').show();
	$('#clear_button').click(function() {
		$('#message_content').val('');
		$('#send_button').prop('disabled', true);
	});
	$('#message_content').on('keyup keydown keypress', function() {
		$('#send_button').prop('disabled', !$(this).val());
	});

	$('[ts][sent-prefix]').each(function() {
		var o = $(this);
		var s = o.attr('sent-prefix') + (new Date(o.attr('ts')*1000)).toLocaleString();
		$(this).html(s);
	});

	$('html,body').animate({
		scrollTop: $('#new-message-form').offset().top
	}, 1000);

});